package se.pilotfish.spartacus.conf.auth;

import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.KeySourceException;
import com.nimbusds.jose.proc.JWSAlgorithmFamilyJWSKeySelector;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.JWTClaimsSetAwareJWSKeySelector;
import java.net.URL;
import java.security.Key;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtIssuerValidator;
import org.springframework.stereotype.Component;
import se.pilotfish.spartacus.repository.CustomerMasterRepository;

/**
 * Adapter for Spring Security Resource Server to support multi-tenant resolution and
 * verification of JWT tokens (see e.g. https://docs.spring.io/spring-security/reference/servlet/oauth2/resource-server/multitenancy.html).
 *
 * Each Customer (as stored by CustomerMaster) is considered a valid authorization realm,
 * where the issuer for the realm is constructed by applying ${issuerUriFormat} to the Customer shortName.
 */
@Component
public class MultiTenantJWSAdapter
    implements JWTClaimsSetAwareJWSKeySelector<SecurityContext>, OAuth2TokenValidator<Jwt> {

  private final String issuerUriFormat;
  private final String jwtSetUriFormat;
  private final Pattern issuerUriPattern;
  private final CustomerMasterRepository tenants;
  private final Map<String, JWSKeySelector<SecurityContext>> selectors = new ConcurrentHashMap<>();
  private final Map<String, JwtIssuerValidator> validators = new ConcurrentHashMap<>();

  private final static String TENANT_GROUP_REGEXP = "([a-zA-Z_0-9]*)";

  public MultiTenantJWSAdapter(
      @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri-format}") String issuerUriFormat,
      @Value("${spring.security.oauth2.resourceserver.jwt.jwk-set-uri-format}") String jwtSetUriFormat,
      CustomerMasterRepository tenants) {
    this.tenants = tenants;
    this.issuerUriFormat = issuerUriFormat;
    this.jwtSetUriFormat = jwtSetUriFormat;
    this.issuerUriPattern = Pattern.compile(MessageFormat.format(issuerUriFormat, TENANT_GROUP_REGEXP));
  }

  /**
   * @See com.nimbusds.jwt.proc.JWTClaimsSetAwareJWSKeySelector#selectKeys(JWSHeader, JWTClaimsSet, SecurityContext)
   */
  @Override
  public List<? extends Key> selectKeys(JWSHeader jwsHeader, JWTClaimsSet jwtClaimsSet, SecurityContext securityContext)
      throws KeySourceException {
    String issuer = (String) jwtClaimsSet.getClaim("iss");
    return this.selectors.computeIfAbsent(toTenant(issuer), this::selectorFromTenant)
        .selectJWSKeys(jwsHeader, securityContext);
  }

  /**
   * @See org.springframework.security.oauth2.core.OAuth2TokenValidator#validate(Jwt)
   */
  @Override
  public OAuth2TokenValidatorResult validate(Jwt token) {
    String issuer = token.getIssuer().toString();
    return this.validators.computeIfAbsent(toTenant(issuer), this::validatorFromTenant)
        .validate(token);
  }


  private String toTenant(String issuer) {
    Matcher matcher = issuerUriPattern.matcher(issuer);
    if (matcher.matches()) {
      return matcher.group(1);
    } else {
      throw new IllegalArgumentException("unknown issuer");
    }
  }

  private JWSKeySelector<SecurityContext> selectorFromTenant(String tenant) {
    return this.tenants.findByShortName(tenant)
        .map(t -> MessageFormat.format(jwtSetUriFormat, t.getShortName()))
        .map(this::selectorFromUri)
        .orElseThrow(() -> new IllegalArgumentException("unknown tenant: " + tenant));
  }

  private JWSKeySelector<SecurityContext> selectorFromUri(String uri) {
    try {
      return JWSAlgorithmFamilyJWSKeySelector.fromJWKSetURL(new URL(uri));
    } catch (Exception ex) {
      throw new IllegalArgumentException(ex);
    }
  }

  private JwtIssuerValidator validatorFromTenant(String tenant) {
    return this.tenants.findByShortName(tenant)
        .map(t -> MessageFormat.format(issuerUriFormat, t.getShortName()))
        .map(JwtIssuerValidator::new)
        .orElseThrow(() -> new IllegalArgumentException("unknown tenant: " + tenant));
  }

}