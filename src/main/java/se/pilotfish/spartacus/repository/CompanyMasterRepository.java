package se.pilotfish.spartacus.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.pilotfish.spartacus.application.db.master.CompanyMaster;

@Repository
public interface CompanyMasterRepository extends CrudRepository<CompanyMaster, UUID> {

  Optional<CompanyMaster> findByCustomerShortNameAndExternalIdentifier(String customerShortName, String externalIdentifier);

}
