package se.pilotfish.spartacus.rest.service.v3;

import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.SpartacusUser;
import se.pilotfish.spartacus.kafka.producer.InternalMessageProducer;
import se.pilotfish.spartacus.rest.versions.Version_1_0;
import se.pilotfish.spartacus.service.ValidationService;
import se.pilotfish.spartacus.util.UUIDGenerator;
import se.pilotfish.spartacusclient.rest.json.Entity;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;

@Slf4j
@Service("entityService-1-0")
@Version_1_0
@Transactional
public class EntityServiceImpl10 implements EntityService {

  @Inject
  ValidationService validationService;

  @Inject
  InternalMessageProducer kafkaProducer;

  public Entity handleEvent(Event event, Entity toSync, String version, Source source, JwtAuthenticationToken authorization) {

    //Context for call.
    SpartacusUser user = new SpartacusUser(authorization);

    //Generate message
    Message message = Message.builder()
        .identifier(UUIDGenerator.generateUUID())
        .event(event)
        .source(source)
        .payload(toSync)
        .build();

    if (log.isInfoEnabled()) {
      log.info("Update {} from {} assigned message-id={}",
          message.getPayloadType(), source, message.getIdentifier());
    }

    return validateAndSend(user, message);

  }

  private Entity validateAndSend(SpartacusUser user, Message message) {

    //Load and check validation rules.

    validationService.validate(message.getPayload());

    user.canUpdate(message.getPayload());

    //Post to topic.
    kafkaProducer.publish(message);

    if (log.isTraceEnabled()) {
      log.trace("Message {} successfully handled.", message.getIdentifier());
    }
    return message.getPayload();
  }

}
