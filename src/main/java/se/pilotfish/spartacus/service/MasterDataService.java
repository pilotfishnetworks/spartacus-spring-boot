package se.pilotfish.spartacus.service;


import se.pilotfish.spartacusclient.rest.json.Message;

public interface MasterDataService {

  Message updateMasterData(Message message);

}
