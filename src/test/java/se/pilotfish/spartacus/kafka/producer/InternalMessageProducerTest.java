package se.pilotfish.spartacus.kafka.producer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import se.pilotfish.spartacusclient.rest.json.Message;

@ExtendWith(MockitoExtension.class)
class InternalMessageProducerTest {

  @Mock
  KafkaTemplate<String, Message> kafkaTemplate;

  @Test
  void publish() {
    InternalMessageProducerImpl messageProducer = new InternalMessageProducerImpl(kafkaTemplate, "topic");

    Message message = Mockito.mock(Message.class);
    Mockito.when(message.messageKey()).thenReturn("KEY");

    messageProducer.publish(message);

    Mockito.verify(kafkaTemplate).send("topic", message.messageKey(), message);

  }
}