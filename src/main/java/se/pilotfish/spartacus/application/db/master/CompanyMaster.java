package se.pilotfish.spartacus.application.db.master;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(uniqueConstraints =
        @UniqueConstraint(name = "CompanyMaster_customerShortName_externalIdentifier_idx",
                          columnNames = {"customerShortName", "externalIdentifier"}))
@Getter
@Setter
@NoArgsConstructor
public class CompanyMaster extends GenericMasterRecord {

    @NotNull
    private String customerShortName;

    @NotNull
    private String externalIdentifier;


    @OneToMany(mappedBy = "masterCompany", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @MapKey
    Map<GenericMasterValueId, CompanyValue> companyValues = new HashMap<>();

    @Builder
    public CompanyMaster(UUID identifier, @NotNull String customerShortName, @NotNull String externalIdentifier, Boolean hidden) {
        super(identifier, hidden);
        this.customerShortName = customerShortName;
        this.externalIdentifier = externalIdentifier;
    }

    @Override
    public Map<GenericMasterValueId, ? extends GenericMasterValue> getValues() {
        return companyValues;
    }

    @Override
    public void addValue(GenericMasterValue updatedValue) {
        CompanyValue updatedCompanyValue = (CompanyValue) updatedValue;
        updatedCompanyValue.setMasterCompany(this);
        companyValues.put(updatedValue.getIdentifier(), updatedCompanyValue);
    }

    @Override
    public void removeValue(GenericMasterValue removedValue) {
        CompanyValue removedCompanyValue =  companyValues.remove(removedValue.getIdentifier());
        if (removedCompanyValue != null) {
            removedCompanyValue.remove();
        }
    }

}
