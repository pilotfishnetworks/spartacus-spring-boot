package se.pilotfish.spartacus.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static se.pilotfish.spartacus.util.UUIDUtil.toUUID;

import com.github.database.rider.core.api.dataset.DataSet;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.DateTimeProvider;
import se.pilotfish.spartacus.annotation.SpringBootDbIntegrationTest;
import se.pilotfish.spartacus.application.db.master.VehicleModelMaster;
import se.pilotfish.spartacus.db.AbstractTestContainerIntegrationTest;

@SpringBootDbIntegrationTest
class VehicleModelMasterRepositoryIntegrationTest extends AbstractTestContainerIntegrationTest {

  @MockBean
  private DateTimeProvider dateTimeProvider;

  @SpyBean
  private AuditingHandler handler;

  @Inject
  private VehicleModelMasterRepository repo;

  @BeforeEach
  @DataSet("datasets/CustomerMaster.yml")
  void setUp() throws Exception {
    handler.setDateTimeProvider(dateTimeProvider);
  }

  @AfterEach
  @DataSet("datasets/clean.yml")
  void teardown() {
  }

  @Test
  @DataSet("datasets/VehicleModelMaster.yml")
  void findByCustomerShortNameAndExternalIdentifier() {
    VehicleModelMaster vehicleModelMaster = repo.findByCustomerShortNameAndExternalIdentifier("company1", "vehicleModelIdentifier").orElse(null);
    assertThat(vehicleModelMaster).isNotNull();
    assertThat(vehicleModelMaster.getIdentifier()).isEqualTo(toUUID(1));
    assertThat(vehicleModelMaster.getVehicleValues().size()).isEqualTo(2);
  }

  @Test
  @DataSet("datasets/VehicleModelMaster.yml")
  void findById() {
    VehicleModelMaster vehicleModelMaster  = repo.findById(toUUID(1)).orElse(null);
    assertThat(vehicleModelMaster).isNotNull();
  }

}