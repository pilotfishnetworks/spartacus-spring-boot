package se.pilotfish.spartacus.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.auditing.AuditingHandler;
import se.pilotfish.spartacus.application.db.master.CompanyMaster;
import se.pilotfish.spartacus.repository.CompanyMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Company;
import se.pilotfish.spartacusclient.rest.json.CustomerReference;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;

@RunWith(MockitoJUnitRunner.class)
class CompanyMasterDataServiceImplTest {

  @Mock
  private AuditingHandler auditingHandler;

  @Mock
  private CompanyMasterRepository companyMasterRepository;

  @InjectMocks
  CompanyMasterDataServiceImpl service;

  @BeforeEach
  public void initMocks() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void updateCustomerMaster() {
    Company companyEntity = Company.builder()
        .customer(CustomerReference.builder().shortName("company1").build())
        .externalIdentifier("companyIdentifier")
        .type("type")
        .build();
    Message message = Message.builder()
        .event(Event.CREATE_OR_UPDATE)
        .source(Source.CUSTOMER)
        .payload(companyEntity)
        .build();

    CompanyMaster companyMaster = CompanyMaster.builder().build();
    LocalDateTime now = LocalDateTime.now();
    companyMaster.setCreated(now);
    companyMaster.setUpdated(now);
    when(companyMasterRepository.findByCustomerShortNameAndExternalIdentifier("company1", "companyIdentifier")).thenReturn(
        Optional.of(companyMaster));
    when(companyMasterRepository.save(companyMaster)).thenReturn(companyMaster);

    service.updateCompanyMaster(message);

    verify(companyMasterRepository).save(eq(companyMaster));
  }

}