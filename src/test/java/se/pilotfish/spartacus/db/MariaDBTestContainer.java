package se.pilotfish.spartacus.db;

import org.testcontainers.containers.MariaDBContainer;

public class MariaDBTestContainer extends MariaDBContainer<MariaDBTestContainer> {
    private static final String IMAGE_VERSION = "mariadb:10.3";

    public MariaDBTestContainer() {
        super(IMAGE_VERSION);
    }

    @Override
    protected void configure() {
        super.configure();
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_NAME", super.getDatabaseName());
        System.setProperty("DB_URL", super.getJdbcUrl());
        System.setProperty("DB_USERNAME", super.getUsername());
        System.setProperty("DB_PASSWORD", super.getPassword());
    }
    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}