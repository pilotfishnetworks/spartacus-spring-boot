package se.pilotfish.spartacus.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.pilotfish.spartacus.application.db.master.VehicleModelMaster;

@Repository
public interface VehicleModelMasterRepository extends CrudRepository<VehicleModelMaster, UUID> {

  Optional<VehicleModelMaster> findByCustomerShortNameAndExternalIdentifier(String customerShortName, String externalIdentifier);

}
