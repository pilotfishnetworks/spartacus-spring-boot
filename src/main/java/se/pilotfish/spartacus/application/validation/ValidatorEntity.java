package se.pilotfish.spartacus.application.validation;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Getter;
import lombok.Setter;
import ognl.Ognl;
import ognl.OgnlException;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import se.pilotfish.spartacus.application.failure.ErrorId;
import se.pilotfish.spartacus.application.failure.ProcessingException;
import se.pilotfish.spartacusclient.rest.json.Entity.EntityType;


@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@Getter
@Setter
public abstract class ValidatorEntity implements Validator {

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID",
      strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "id", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
  @Type(type = "uuid-char")
  private UUID id;

  /**
   * Property that this validator can validate.
   * Support dot and square brackets syntax. Dots to
   * reach a bean property and square brackets access
   * maps or arrays/lists.
   * <br>
   * Examples: bean.map["key"], bean.list[1]
   *
   */
  private String property;

  /**
   * Basic configuration for this validator.
   */
  private String configuration;

  /**
   * Company shortname that this validator is active for
   */
  private String validatesFor;

  /**
   * Type of entity that this validator can validate
   */
  @Enumerated(EnumType.STRING)
  private EntityType validates;

  /**
   * Error message to display when validation failed.
   *
   */
  private String description;

  /**
   * Fetches the value to be validated.
   *
   * @param entity object that is being validated.
   * @return the value at property path in entity
   */
  public Object fetchProperty(se.pilotfish.spartacusclient.rest.json.Entity entity) {

    try {
      return Ognl.getValue(property, entity);
    } catch (OgnlException e) {
      throw new ProcessingException(ErrorId.INVALID_VALUE,
          String.format("Property path %s could not processed on entity",
              property), e);
    }

  }

  @Override
  public ValidationError validate(se.pilotfish.spartacusclient.rest.json.Entity entity) {
    if(!isValid(entity)) {
      return generateValidationError();
    }
    return null;
  }

  public ValidationError generateValidationError() {
    return new ValidationError(property, description);
  }

}
