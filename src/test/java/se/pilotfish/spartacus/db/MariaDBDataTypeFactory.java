package se.pilotfish.spartacus.db;

import java.util.Arrays;
import java.util.Collection;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;

/**
 * Specialized factory that recognizes MySql data types.
 *
 * @author manuel.laflamme
 * @author Last changed by: $Author$
 * @version $Revision$ $Date$
 * @since 1.5 (Sep 3, 2003)
 */
public class MariaDBDataTypeFactory extends MySqlDataTypeFactory {
  /**
   * Database product names supported.
   */
  private static final Collection DATABASE_PRODUCTS = Arrays.asList(new String[] {"mariadb"});
  /**
   * @see org.dbunit.ext.mysql.MySqlDataTypeFactory#getValidDbProducts()
   */
  @Override
  public Collection getValidDbProducts()
  {
    return DATABASE_PRODUCTS;
  }

  /**
   * @see org.dbunit.ext.mysql.MySqlDataTypeFactory#createDataType(int, String)
   */
  @Override
  public DataType createDataType(int sqlType, String sqlTypeName) throws DataTypeException {
    // Treat BIT as Boolean
    if ("bit".equalsIgnoreCase(sqlTypeName)) {
      return DataType.BOOLEAN;
    } else {
      return super.createDataType(sqlType, sqlTypeName);
    }
  }
}
