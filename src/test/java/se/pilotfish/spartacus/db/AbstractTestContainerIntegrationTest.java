package se.pilotfish.spartacus.db;

import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public abstract class AbstractTestContainerIntegrationTest {

    @Container
    private static final MariaDBTestContainer POSTGRESQL_TEST_CONTAINER = new MariaDBTestContainer()
            .withUsername("mariadb")
            .withPassword("mariadb")
            .withDatabaseName("testdb");

}
