package se.pilotfish.spartacus.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import se.pilotfish.spartacusclient.rest.json.Company;
import se.pilotfish.spartacusclient.rest.json.Customer;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.User;
import se.pilotfish.spartacusclient.rest.json.Vehicle;

@RunWith(MockitoJUnitRunner.class)
class MasterDataServiceImplTest {

  @Mock
  private CompanyMasterDataService companyMasterDataService;

  @Mock
  private CustomerMasterDataService customerMasterDataService;

  @Mock
  private UserMasterDataService userMasterDataService;

  @Mock
  private VehicleMasterDataService vehicleMasterDataService;

  @InjectMocks
  MasterDataServiceImpl masterDataService;

  @BeforeEach
  public void initMocks() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void updateCompanyMasterData() {
    Company companyEntity = new Company();
    companyEntity.setExternalIdentifier("identifier");
    Message message = Message.builder()
        .payload(companyEntity)
        .event(Event.UPDATE)
        .source(Source.CUSTOMER)
        .build();

    masterDataService.updateMasterData(message);

    Mockito.verify(companyMasterDataService, Mockito.only()).updateCompanyMaster(message);
  }

  @Test
  void updateCustomerMasterData() {
    Customer customerEntity = new Customer();
    customerEntity.setShortName("customer");
    Message message = Message.builder()
        .payload(customerEntity)
        .event(Event.UPDATE)
        .source(Source.CUSTOMER)
        .build();

    masterDataService.updateMasterData(message);

    Mockito.verify(customerMasterDataService, Mockito.only()).updateCustomerMaster(message);
  }


  @Test
  void updateUserMasterData() {
    User userEntity = new User();
    userEntity.setExternalIdentifier("identifier");
    Message message = Message.builder()
        .payload(userEntity)
        .event(Event.UPDATE)
        .source(Source.CUSTOMER)
        .build();

    masterDataService.updateMasterData(message);

    Mockito.verify(userMasterDataService, Mockito.only()).updateUserMaster(message);
  }

  @Test
  void updateVehicleMasterData() {
    Vehicle vehicleEntity = new Vehicle();
    vehicleEntity.setExternalIdentifier("identifier");
    Message message = Message.builder()
        .payload(vehicleEntity)
        .event(Event.UPDATE)
        .source(Source.CUSTOMER)
        .build();

    masterDataService.updateMasterData(message);

    Mockito.verify(vehicleMasterDataService, Mockito.only()).updateVehicleMaster(message);
  }

}