package se.pilotfish.spartacus.conf.topic;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "kafka.topic.master-data")
public class MasterDataTopicProperties extends TopicProperties{
}
