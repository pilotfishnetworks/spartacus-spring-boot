package se.pilotfish.spartacus.kafka.producer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.Map;
import javax.inject.Inject;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit4.SpringRunner;
import se.pilotfish.spartacusclient.rest.json.Company;
import se.pilotfish.spartacusclient.rest.json.CustomerReference;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;

@RunWith(SpringRunner.class)
@SpringBootTest
@Tag("integration")
public class InternalMessageProducerIntegrationTest {

  @Inject
  private ObjectMapper mapper;

  @Inject
  private InternalMessageProducer internalMessageProducer;

  @Inject
  private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

  private Consumer<String, Message> consumer;

  public static final String TOPIC = "spartacus-incoming-masterdata";

  @ClassRule
  public static EmbeddedKafkaRule embeddedKafkaRule = new EmbeddedKafkaRule(1, true,
      TOPIC);

  @BeforeClass
  public static void setUpBeforeClass() {
      System.setProperty("spring.kafka.bootstrap-servers", embeddedKafkaRule.getEmbeddedKafka().getBrokersAsString());
  }

  @Before
  public void setUp() {
    Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("testGroup", "true", embeddedKafkaRule.getEmbeddedKafka());
    consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    JsonDeserializer<Message> jsonDeserializer = new JsonDeserializer<>(mapper);
    jsonDeserializer.addTrustedPackages(Message.class.getPackage().getName());
    consumer = new DefaultKafkaConsumerFactory<String, Message>(consumerProps, new StringDeserializer(), jsonDeserializer)
        .createConsumer();
    consumer.subscribe(Collections.singleton(TOPIC));
  }

  @After
  public void tearDown() {
    consumer.close();
  }

  @Test
  public void testMessageProducer() throws Exception {
    Company company = Company.builder()
        .customer(CustomerReference.builder().shortName("customer").build())
        .externalIdentifier("company")
        .build();
    Message message = Message.builder()
        .event(Event.CREATE)
        .source(Source.CUSTOMER)
        .payload(company)
        .build();

    internalMessageProducer.publish(message);

    ConsumerRecord<String, Message> received = KafkaTestUtils.getSingleRecord(consumer, TOPIC);
    assertThat(received).isNotNull();
    assertThat(mapper.writeValueAsString(received.value())).isEqualTo(mapper.writeValueAsString(message));
    assertThat(received.value()).isEqualTo(message);
    assertThat(received.key()).isEqualTo(message.getPayload().getIdentifier());
  }

}