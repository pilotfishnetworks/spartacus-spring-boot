package se.pilotfish.spartacus.service;

import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.BASIC_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.IDENTIFIER_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.PROPERTY_CLASSIFICATION;

import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.db.master.ValueType;
import se.pilotfish.spartacus.application.db.master.VehicleMaster;
import se.pilotfish.spartacus.application.db.master.VehicleValue;
import se.pilotfish.spartacus.application.failure.EntityGoneException;
import se.pilotfish.spartacus.repository.VehicleMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.Vehicle;

@Slf4j
@Service
public class VehicleMasterDataServiceImpl implements VehicleMasterDataService {

  @Inject
  private AuditingHandler auditingHandler;

  @Inject
  private VehicleMasterRepository vehicleMasterRepository;

  @Override
  @Transactional
  public Message updateVehicleMaster(Message message) {
    Vehicle vehicle = (Vehicle) message.getPayload();
    VehicleMaster vehicleMaster = null;
    switch (message.getEvent()) {
      case CREATE:
        vehicleMaster = createVehicleMaster(vehicle);
        break;
      case UPDATE:
        vehicleMaster = vehicleMasterRepository.findById(vehicle.getMasterId())
            .orElseThrow(() -> new EntityGoneException("No such Vehicle: "+ vehicle.getMasterId(), null));
        break;
      case CREATE_OR_UPDATE:
        message.setEvent(Event.UPDATE);
        vehicleMaster = vehicleMasterRepository.findByCustomerShortNameAndExternalIdentifier(
                vehicle.customerShortname(), vehicle.getExternalIdentifier())
            .orElseGet(() -> {
              message.setEvent(Event.CREATE);
              return createVehicleMaster(vehicle);
            });
        break;
      default:
        log.error("Unsupported event type {}", message.getEvent());
        return message;
    }
    Set<VehicleValue> updatedValues = asVehicleValues(vehicle, message.getSource());
    vehicleMaster.updateFrom(updatedValues);
    // Mark vehicleMaster as updated, even though only its related values may have changed
    auditingHandler.markModified(vehicleMaster);
    vehicleMaster = vehicleMasterRepository.save(vehicleMaster);
    vehicle.getMeta().setCreated(vehicleMaster.getCreated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    vehicle.getMeta().setUpdated(vehicleMaster.getUpdated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    return message;
  }

  private VehicleMaster createVehicleMaster(Vehicle vehicle) {
    return VehicleMaster.builder()
        .customerShortName(vehicle.customerShortname())
        .externalIdentifier(vehicle.getExternalIdentifier())
        .build();
  }

  private Set<VehicleValue> asVehicleValues(Vehicle updatedVehicle, Source source) {
    Set<VehicleValue> updatedValues = new HashSet<>();
    updatedValues.add(new VehicleValue(IDENTIFIER_CLASSIFICATION, "externalIdentifier", updatedVehicle.getExternalIdentifier(), source));
    updatedValues.add(new VehicleValue(BASIC_CLASSIFICATION, "registrationNumber", updatedVehicle.getRegistrationNumber(), source));
    updatedValues.add(new VehicleValue(BASIC_CLASSIFICATION, "vin", updatedVehicle.getVin(), source));
    updatedValues.add(new VehicleValue(BASIC_CLASSIFICATION, "vehicleNo", updatedVehicle.getVehicleNumber(), source));
    if (updatedVehicle.getState() != null) {
      updatedValues.add(new VehicleValue(BASIC_CLASSIFICATION, "state", updatedVehicle.getState().toString(),
          source));
    }
    if (updatedVehicle.getProperties() != null) {
      updatedVehicle.getProperties().forEach(p ->
          updatedValues.add(new VehicleValue(PROPERTY_CLASSIFICATION, p.getName(), p.getValue(), ValueType.valueOf(p.getType()),
              source)));
    }
    return updatedValues;
  }

}
