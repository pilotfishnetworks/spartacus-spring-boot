package se.pilotfish.spartacus.service;

import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.BASIC_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.PROPERTY_CLASSIFICATION;

import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.db.master.CompanyMaster;
import se.pilotfish.spartacus.application.db.master.CompanyValue;
import se.pilotfish.spartacus.application.db.master.ValueType;
import se.pilotfish.spartacus.application.failure.EntityGoneException;
import se.pilotfish.spartacus.repository.CompanyMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Company;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;

@Slf4j
@Service
public class CompanyMasterDataServiceImpl implements CompanyMasterDataService {

  @Inject
  private AuditingHandler auditingHandler;

  @Inject
  private CompanyMasterRepository companyMasterRepository;

  @Override
  @Transactional
  public Message updateCompanyMaster(Message message) {
    Company company = (Company) message.getPayload();
    CompanyMaster companyMaster = null;
    switch (message.getEvent()) {
      case CREATE:
        companyMaster = createCompanyMaster(company);
        break;
      case UPDATE:
        companyMaster = companyMasterRepository.findByCustomerShortNameAndExternalIdentifier(
            company.customerShortname(), company.getExternalIdentifier())
            .orElseThrow(() -> new EntityGoneException("No such Company: "+ company.getExternalIdentifier(), null));
        break;
      case CREATE_OR_UPDATE:
        message.setEvent(Event.UPDATE);
        companyMaster = companyMasterRepository.findByCustomerShortNameAndExternalIdentifier(
            company.customerShortname(), company.getExternalIdentifier())
            .orElseGet(() -> {
              message.setEvent(Event.CREATE);
              return createCompanyMaster(company);
            });
        break;
      default:
        log.error("Unsupported event type {}", message.getEvent());
        return message;
    }
    Set<CompanyValue> updatedValues = asCompanyValues(company, message.getSource());
    companyMaster.updateFrom(updatedValues);
    // Mark companyMaster as updated, even though only its related values may have changed
    auditingHandler.markModified(companyMaster);
    companyMaster =  companyMasterRepository.save(companyMaster);
    company.getMeta().setCreated(companyMaster.getCreated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    company.getMeta().setUpdated(companyMaster.getUpdated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    return message;
  }

  private CompanyMaster createCompanyMaster(Company company) {
    return CompanyMaster.builder()
        .customerShortName(company.customerShortname())
        .externalIdentifier(company.getExternalIdentifier())
        .build();
  }

  private Set<CompanyValue> asCompanyValues(Company updatedCompany, Source source) {
    Set<CompanyValue> updatedValues = new HashSet<>();
    updatedValues.add(new CompanyValue(BASIC_CLASSIFICATION, "name", updatedCompany.getName(), source));
    updatedValues.add(new CompanyValue(BASIC_CLASSIFICATION, "type", updatedCompany.getType(), source));
    updatedValues.add(new CompanyValue(BASIC_CLASSIFICATION, "parent", updatedCompany.getParentIdentifier(),
        source));
    if (updatedCompany.getLocale() != null) {
      updatedValues.add(new CompanyValue(BASIC_CLASSIFICATION, "locale", updatedCompany.getLocale(), source));
    }
    if (updatedCompany.getTimeZone() != null) {
      updatedValues.add(new CompanyValue(BASIC_CLASSIFICATION, "timeZone", updatedCompany.getTimeZone(), source));
    }
    if (updatedCompany.getState() != null) {
      updatedValues.add(new CompanyValue(BASIC_CLASSIFICATION, "state", updatedCompany.getState().toString(),
          source));
    }
    if (updatedCompany.getProperties() != null) {
      updatedCompany.getProperties().forEach(p ->
          updatedValues.add(new CompanyValue(PROPERTY_CLASSIFICATION, p.getName(), p.getValue(), ValueType.valueOf(p.getType()),
              source)));
    }
    return updatedValues;
  }

}
