package se.pilotfish.keycloak;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.springframework.security.oauth2.jwt.Jwt;

public class TokenParser {

    public static final String SHORTNAME_KEY = "company_shortname";
    public static final String EXTRAS_KEY = "wanda_extras";
    public static final String ROLES_KEY = "wanda_roles";
    public static final String GROUPS_KEY = "wanda_groups";

    String shortname = "##NO-SHORTNAME##";
    Map<String, List<Long>> wandaGroups = new HashMap<>();
    Map<String, List<Long>> assignedRoles = new HashMap<>();

    public Map<String, List<Long>> getWandaGroups() {
        return wandaGroups;
    }

    public void setWandaGroups(Map<String, List<Long>> wandaGroups) {
        this.wandaGroups = wandaGroups;
    }

    public Map<String, List<Long>> getAssignedRoles() {
        return assignedRoles;
    }

    public void setAssignedRoles(Map<String, List<Long>> assignedRoles) {
        this.assignedRoles = assignedRoles;
    }


    public List<Long> assignedTo(String roleName) {
        List<Long> companyIds = assignedRoles.get(roleName);
        if (companyIds == null) {
            return Collections.emptyList();
        }
        return companyIds;
    }

    public List<Long> canHandle(String role) {
        List<Long> canHandleCompanies = wandaGroups.get(role);

        if (canHandleCompanies == null) {
            return Collections.emptyList();
        }
        return canHandleCompanies;
    }

    public boolean hasRole(String role){
        for (String entry : assignedRoles.keySet()){
            if(entry.equals(role)){
                return true;
            }
        }
        return false;
    }

    public boolean hasRoleAndCompanyAccess(String role, String companyId){
        Long internalCompanyId = Long.parseLong(companyId);
        List<Long> assignedCompaniesForThisRole = wandaGroups.get(role);

        if(assignedCompaniesForThisRole == null){
            return false;
        }

        for(Long entry : assignedCompaniesForThisRole){
            if(entry.equals(internalCompanyId)){
                return true;

            }
        }
        return false;
    }

    public List<String> giveMeMyRoles(){
        Set<String> assignedRoleNames = assignedRoles.keySet();
        List<String> roleNameList = new LinkedList<>();

        for (String roleName : assignedRoleNames){
            roleNameList.add(roleName);
        }
        return roleNameList;
    }

    public void parseJwt(Jwt jwt) {
        Map<String, Object> wandaRolesClaim = jwt.getClaimAsMap(ROLES_KEY);
        if (wandaRolesClaim == null) {
            throw new NullPointerException("wanda_roles claim is null");
        }

        Map<String, Object> wandaGroupClaim = jwt.getClaimAsMap(GROUPS_KEY);
        if (wandaGroupClaim == null) {
            throw new NullPointerException("wanda_groups claim is null");
        }

        Map<String, Object> extrasClaim = jwt.getClaimAsMap(EXTRAS_KEY);
        if (extrasClaim != null) {
            if (extrasClaim.containsKey(SHORTNAME_KEY)) {
               shortname = (String) extrasClaim.get(SHORTNAME_KEY);
            }
        }

        for (Object current : wandaRolesClaim.entrySet()) {
            @SuppressWarnings("unchecked")
            Map.Entry<String, List<Long>> roleClaimData = (Map.Entry<String, List<Long>>) current;
            List<WandaRoleClaim> roleClaims = WandaRoleClaim.from(roleClaimData);

            List<Long> roleCompanies = new LinkedList<>();

            for (WandaRoleClaim roleClaim : roleClaims) {
                String role = roleClaim.role;
                Long roleCompany = roleClaim.companyId;

                roleCompanies.add(roleCompany);

                assignedRoles.put(role, roleCompanies);
            }
        }

        wandaGroups = WandaGroupClaim.from(wandaGroupClaim, assignedRoles);

    }

    public boolean hasRoleAndShortnameAccess(String role, String shortname) {
        List<Long> assignedCompaniesForThisRole = wandaGroups.get(role);
        if(assignedCompaniesForThisRole == null){
            return false;
        }
        return hasAccessToShortName(shortname);
    }

    public boolean hasAccessToShortName(String shortname) {
        return this.shortname.equals(shortname);
    }

    private static class WandaRoleClaim {

        String role;
        Long companyId;

        WandaRoleClaim(String role, Long fromJwt) {
            this.role = role;
            this.companyId = fromJwt;

        }

        public static List<WandaRoleClaim> from(Map.Entry<String, List<Long>> roleClaimData) {
            List<WandaRoleClaim> result = new LinkedList<>();
            try {
                //Put them in a Set to remove duplicates
                Set<Long> uniqueCompanyIds = new HashSet<>();
                uniqueCompanyIds.addAll(roleClaimData.getValue());

                String role = roleClaimData.getKey();
                for (Long companyId : uniqueCompanyIds) {
                    result.add(new WandaRoleClaim(role, companyId));
                }

                return result;
            } catch (NoSuchElementException exc) {
                //LogManager.getLogger(TokenParser.class).warn("Ignoring unknown role name " + roleClaimData.getKey());
                return result;
            }
        }
    }

    private static class WandaGroupClaim {

        Long companyId;
        List<WandaGroupClaim> children = new LinkedList<>();

        WandaGroupClaim(Long group) {
            this.companyId = group;
        }

        public static Map<String, List<Long>> from(Map<String, Object> wandaGroupClaim,
                                                        Map<String, List<Long>> assignedRoles){

            Map<Long, List<Long>> parentChildrenRelationship = new HashMap<>();

            //First loop for making the strings to Longs
            for(Object current : wandaGroupClaim.entrySet()){
                @SuppressWarnings("unchecked")
                Map.Entry<String, List<Long>> groupClaimData = (Map.Entry<String, List<Long>>) current;
                parentChildrenRelationship.put(Long.parseLong(groupClaimData.getKey()), groupClaimData.getValue());
            }

            Map<Long, WandaGroupClaim> allClaimsFromId = new HashMap<>();

            //Second and third loop for making the tree structure
            for(Map.Entry<Long, List<Long>> relationship : parentChildrenRelationship.entrySet()){

                WandaGroupClaim existingParent = allClaimsFromId.get(relationship.getKey());
                if(existingParent == null){
                    existingParent = new WandaGroupClaim(relationship.getKey());
                    allClaimsFromId.put(existingParent.companyId, existingParent);
                }

                for(Long childId : relationship.getValue()){

                    WandaGroupClaim existingChild = allClaimsFromId.get(childId);
                    if(existingChild == null){
                        existingChild = new WandaGroupClaim(childId);
                        allClaimsFromId.put(childId, existingChild);
                    }

                    existingParent.addChild(existingChild);
                }

            }

            Map<String, List<Long>> endResult = new HashMap<>();

            //Fourth loop for adding role and their subnodes
            for(Map.Entry<String, List<Long>> assignedNodes : assignedRoles.entrySet()){
                String key = assignedNodes.getKey();
                Set<Long> nodes = new HashSet<>();
                for(Long topNodes : assignedNodes.getValue()){
                    WandaGroupClaim groupClaim = allClaimsFromId.get(topNodes);
                    if(groupClaim != null) {
                        List<Long> treeNodes = groupClaim.meAndAndAllMyChildrensIds();
                        nodes.addAll(treeNodes);
                    } else {
                        nodes.add(topNodes);
                    }
                }
                endResult.put(key, new LinkedList<>(nodes));
            }

            return endResult;

        }

        private void addChild(WandaGroupClaim child) {
            children.add(child);
        }

        public List<Long> meAndAndAllMyChildrensIds(){
            List<Long> result = new LinkedList<>();
            meAndAndAllMyChildrenIds(result);
            return result;
        }

        public void meAndAndAllMyChildrenIds(List<Long> result){
            result.add(this.companyId);
            for(WandaGroupClaim child : children){
                child.meAndAndAllMyChildrenIds(result);
            }
        }
    }
}