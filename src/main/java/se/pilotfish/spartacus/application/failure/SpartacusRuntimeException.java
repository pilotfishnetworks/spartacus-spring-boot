package se.pilotfish.spartacus.application.failure;

import org.springframework.http.HttpStatus;

public class SpartacusRuntimeException extends RuntimeException {

    private final ErrorId id;

    public SpartacusRuntimeException(ErrorId id, String message, Throwable cause){
        super(message, cause);
        this.id = id;
    }

    public ErrorId getErrorId(){
        return id;
    }

    public int getStatusCode(){
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }


}
