package se.pilotfish.spartacus.rest.versions;

import java.lang.annotation.RetentionPolicy;
import javax.inject.Qualifier;

/**
 * Created by magnusl on 2017-01-23.
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Version_1_0 {
}
