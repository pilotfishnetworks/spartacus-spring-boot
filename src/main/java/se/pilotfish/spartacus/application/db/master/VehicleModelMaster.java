package se.pilotfish.spartacus.application.db.master;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(uniqueConstraints =
        @UniqueConstraint(name = "VehicleModelMaster_customerShortName_externalIdentifier_idx",
                          columnNames = {"customerShortName", "externalIdentifier"}))
@Getter
@Setter
@NoArgsConstructor
public class VehicleModelMaster extends GenericMasterRecord {

    @NotNull
    private String customerShortName;

    private String externalIdentifier;

    @OneToMany(mappedBy = "masterVehicleModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @MapKey
    private Map<GenericMasterValueId, VehicleModelValue> vehicleValues = new HashMap<>();

    @Builder
    public VehicleModelMaster(UUID identifier, @NotNull String customerShortName, String externalIdentifier, Boolean hidden) {
        super(identifier, hidden);
        this.customerShortName = customerShortName;
        this.externalIdentifier = externalIdentifier;
    }

    @Override
    public Map<GenericMasterValueId, ? extends GenericMasterValue> getValues() {
        return vehicleValues;
    }

    @Override
    public void addValue(GenericMasterValue updatedValue) {
        VehicleModelValue updatedVehicleValue = (VehicleModelValue) updatedValue;
        updatedVehicleValue.setMasterVehicleModel(this);
        vehicleValues.put(updatedValue.getIdentifier(), updatedVehicleValue);
    }

    @Override
    public void removeValue(GenericMasterValue removedValue) {
        VehicleModelValue removedVehicleValue =  vehicleValues.remove(removedValue.getIdentifier());
        if (removedVehicleValue != null) {
            removedVehicleValue.remove();
        }
    }

}
