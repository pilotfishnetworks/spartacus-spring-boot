package se.pilotfish.spartacus.application.failure;

/**
 * Signals error in the data already in the system. (Apart from ValidationException that signals
 * errors in input).
 */
public class CorruptDataException extends SpartacusRuntimeException {

    public CorruptDataException(ErrorId id, String message){
        super(id, message, null);
    }

    @Override
    public int getStatusCode(){
        return 520;
    }
}
