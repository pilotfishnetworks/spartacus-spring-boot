package se.pilotfish.spartacus.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static se.pilotfish.spartacus.util.UUIDUtil.toUUID;

import com.github.database.rider.core.api.dataset.DataSet;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.DateTimeProvider;
import se.pilotfish.spartacus.annotation.SpringBootDbIntegrationTest;
import se.pilotfish.spartacus.application.db.master.UserMaster;
import se.pilotfish.spartacus.db.AbstractTestContainerIntegrationTest;

@SpringBootDbIntegrationTest
class UserMasterRepositoryIntegrationTest extends AbstractTestContainerIntegrationTest {

  @MockBean
  private DateTimeProvider dateTimeProvider;

  @SpyBean
  private AuditingHandler handler;

  @Inject
  private UserMasterRepository repo;

  @BeforeEach
  @DataSet("datasets/CustomerMaster.yml")
  void setUp() throws Exception {
    handler.setDateTimeProvider(dateTimeProvider);
  }

  @AfterEach
  @DataSet("datasets/clean.yml")
  void teardown() {
  }

  @Test
  @DataSet("datasets/UserMaster.yml")
  void findByCustomerShortNameAndExternalIdentifier() {
    UserMaster userMaster = repo.findByCustomerShortNameAndExternalIdentifier("company1", "userIdentifier").orElse(null);
    assertThat(userMaster).isNotNull();
  }

  @Test
  @DataSet("datasets/UserMaster.yml")
  void findById() {
    UserMaster userMaster  = repo.findById(toUUID(1)).orElse(null);
    assertThat(userMaster).isNotNull();
    assertThat(userMaster.getIdentifier()).isEqualTo(toUUID(1));
    assertThat(userMaster.getUserValues().size()).isEqualTo(2);
  }

}