package se.pilotfish.spartacus.rest.service.v3;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import se.pilotfish.spartacus.rest.versions.Version_1_0;
import se.pilotfish.spartacusclient.rest.json.Entity;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Source;

@SecurityRequirement(name = "bearerAuth")
public class AbstractEntityController<T extends Entity> {

  @Inject
  protected VersionHandler versionHandler;

  @Inject
  protected ObjectMapper objectMapper;

  @Inject
  @Version_1_0
  protected EntityService version10;

  @PostConstruct
  public void init(){
    versionHandler.register("LATEST", version10);
    versionHandler.register("1_0", version10);
  }

  @Operation(summary = "Lists available minor versions of the api. Replies can be used "+
      "as \"api-version\" parameter to other methods to select implementation.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = String.class))))})
  @GetMapping(value = "/versions",
      produces = {"application/json;charset=utf-8"}
  )
  public Set<String> listApiVersions() {
    return versionHandler.listApiVersions();
  }

  /**
   * Delegate to requested version of EntityService to carry out the operation.
   * @param event The event type
   * @param toSync new/updated data for entity
   * @param version version of the api
   * @param source originator of the update
   * @param authorization authorizationn token
   * @return the created/updated entity
   */
  protected T handleEvent(
      Event event,
      T toSync,
      String version,
      Source source,
      JwtAuthenticationToken authorization) {
    EntityService implementation
        = versionHandler.getVersion(version);
    toSync.getMeta().setVersion(version);
    return (T) implementation.handleEvent(event, toSync, version, source, authorization);
  }

}
