package se.pilotfish.spartacus.service;

import se.pilotfish.spartacusclient.rest.json.Message;

public interface CustomerMasterDataService {

  Message updateCustomerMaster(Message message);

}
