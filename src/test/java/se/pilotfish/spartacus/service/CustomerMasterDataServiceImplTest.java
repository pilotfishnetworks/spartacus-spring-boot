package se.pilotfish.spartacus.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.auditing.AuditingHandler;
import se.pilotfish.spartacus.application.db.master.CustomerMaster;
import se.pilotfish.spartacus.repository.CustomerMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Customer;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;

@RunWith(MockitoJUnitRunner.class)
class CustomerMasterDataServiceImplTest {

  @Mock
  private AuditingHandler auditingHandler;

  @Mock
  private CustomerMasterRepository customerMasterRepository;

  @InjectMocks
  CustomerMasterDataServiceImpl service;

  @BeforeEach
  public void initMocks() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void updateCustomerMaster() {
    Customer customerEntity = Customer.builder()
        .shortName("company1")
        .build();
    Message message = Message.builder()
        .event(Event.CREATE_OR_UPDATE)
        .source(Source.CUSTOMER)
        .payload(customerEntity)
        .build();
    CustomerMaster customerMaster = CustomerMaster.builder().build();
    LocalDateTime now = LocalDateTime.now();
    customerMaster.setCreated(now);
    customerMaster.setUpdated(now);
    when(customerMasterRepository.findByShortName("company1")).thenReturn(
        Optional.of(customerMaster));
    when(customerMasterRepository.save(customerMaster)).thenReturn(customerMaster);

    service.updateCustomerMaster(message);

    verify(customerMasterRepository).save(eq(customerMaster));
  }

}