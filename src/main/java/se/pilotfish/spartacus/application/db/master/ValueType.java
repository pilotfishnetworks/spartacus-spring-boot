package se.pilotfish.spartacus.application.db.master;

/**
 * Created by magnusl on 2018-06-08.
 */
public enum ValueType {
    STRING, DATE, INTEGER, DOUBLE, BOOLEAN, LONG;
}
