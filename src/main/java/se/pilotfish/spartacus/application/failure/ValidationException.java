package se.pilotfish.spartacus.application.failure;

import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import se.pilotfish.spartacus.application.validation.ValidationError;

/**
 * Created by magnusl on 2017-03-08.
 */
public class ValidationException extends SpartacusRuntimeException {

    @Getter
    private final List<ValidationError> errors;

    public ValidationException(ErrorId id, String message){
        this(id, message, new LinkedList<>());
    }

    public ValidationException(ErrorId id, String message, List<ValidationError> errors){
        super(id, message, null);
        this.errors = errors;
    }

    @Override
    public int getStatusCode(){
        return 422;
    }
}
