package se.pilotfish.spartacus.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import se.pilotfish.spartacus.application.validation.ValidatorEntity;
import se.pilotfish.spartacusclient.rest.json.Entity.EntityType;

public interface ValidatorRepository extends CrudRepository<ValidatorEntity, UUID> {

  List<ValidatorEntity> findByValidatesForAndValidates(String shortName, EntityType entityType);

}
