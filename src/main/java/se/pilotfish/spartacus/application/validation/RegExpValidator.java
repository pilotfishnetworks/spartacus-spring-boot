package se.pilotfish.spartacus.application.validation;

import java.util.regex.Pattern;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("reg-exp")
public class RegExpValidator extends ValidatorEntity {

  @Override
  public boolean isValid(se.pilotfish.spartacusclient.rest.json.Entity entity) {

    Object value = fetchProperty(entity);
    if (value != null) {
      return Pattern.compile(getConfiguration()).matcher(value.toString()).matches();
    }
    return false;
  }

}
