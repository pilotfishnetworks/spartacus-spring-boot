package se.pilotfish.spartacus.annotation;

import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.junit5.api.DBRider;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.testcontainers.junit.jupiter.Testcontainers;
import se.pilotfish.spartacus.db.MariaDBDataTypeFactory;

@SpringBootIntegrationTest
@Testcontainers
@DBRider
@DBUnit(dataTypeFactoryClass = MariaDBDataTypeFactory.class, caseSensitiveTableNames=true, allowEmptyFields = true)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SpringBootDbIntegrationTest {
}
