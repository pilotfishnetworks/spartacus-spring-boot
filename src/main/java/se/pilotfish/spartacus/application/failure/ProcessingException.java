package se.pilotfish.spartacus.application.failure;

/**
 * Generic runtime exception for Spartacus.
 */
public class ProcessingException extends SpartacusRuntimeException {

    public ProcessingException(ErrorId id, String message, Throwable cause){
        super(id, message, cause);
    }

}
