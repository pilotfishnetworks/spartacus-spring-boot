package se.pilotfish.spartacus.service;

import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.BASIC_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.IDENTIFIER_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.PROPERTY_CLASSIFICATION;

import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.db.master.UserMaster;
import se.pilotfish.spartacus.application.db.master.UserValue;
import se.pilotfish.spartacus.application.db.master.ValueType;
import se.pilotfish.spartacus.application.failure.EntityGoneException;
import se.pilotfish.spartacus.repository.UserMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.User;

@Slf4j
@Service
public class UserMasterDataServiceImpl implements UserMasterDataService {

  @Inject
  private AuditingHandler auditingHandler;

  @Inject
  private UserMasterRepository userMasterRepository;

  @Override
  @Transactional
  public Message updateUserMaster(Message message) {
    User user = (User) message.getPayload();
    UserMaster userMaster = null;
    switch (message.getEvent()) {
      case CREATE:
        userMaster = createUserMaster(user);
        break;
      case UPDATE:
        userMaster = userMasterRepository.findById(user.getMasterId())
            .orElseThrow(() -> new EntityGoneException("No such User: "+ user.getMasterId(), null));
        break;
      case CREATE_OR_UPDATE:
        message.setEvent(Event.UPDATE);
        userMaster = userMasterRepository.findByCustomerShortNameAndExternalIdentifier(
                user.customerShortname(), user.getExternalIdentifier())
            .orElseGet(() -> {
              message.setEvent(Event.CREATE);
              return createUserMaster(user);
            });
        break;
      default:
        log.error("Unsupported event type {}", message.getEvent());
        return message;
    }
    Set<UserValue> updatedValues = asUserValues(user, message.getSource());
    userMaster.updateFrom(updatedValues);
    // Mark userMaster as updated, even though only its related values may have changed
    auditingHandler.markModified(userMaster);
    userMaster = userMasterRepository.save(userMaster);
    user.getMeta().setCreated(userMaster.getCreated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    user.getMeta().setUpdated(userMaster.getUpdated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    return message;
  }

  private UserMaster createUserMaster(User user) {
    return UserMaster.builder()
        .customerShortName(user.customerShortname())
        .externalIdentifier(user.getExternalIdentifier())
        .build();
  }

  private Set<UserValue> asUserValues(User updatedUser, Source source) {
    Set<UserValue> updatedValues = new HashSet<>();
    updatedValues.add(new UserValue(IDENTIFIER_CLASSIFICATION, "externalIdentifier", updatedUser.getExternalIdentifier(), source));
    updatedValues.add(new UserValue(IDENTIFIER_CLASSIFICATION, "firstName", updatedUser.getFirstName(), source));
    updatedValues.add(new UserValue(IDENTIFIER_CLASSIFICATION, "lastName", updatedUser.getLastName(), source));
    updatedValues.add(new UserValue(IDENTIFIER_CLASSIFICATION, "userName", updatedUser.getUsername(), source));
    if (updatedUser.getState() != null) {
      updatedValues.add(new UserValue(BASIC_CLASSIFICATION, "state", updatedUser.getState().toString(),
          source));
    }
    if (updatedUser.getProperties() != null) {
      updatedUser.getProperties().forEach(p ->
          updatedValues.add(new UserValue(PROPERTY_CLASSIFICATION, p.getName(), p.getValue(), ValueType.valueOf(p.getType()),
              source)));
    }
    return updatedValues;
  }



}
