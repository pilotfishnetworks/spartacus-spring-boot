package se.pilotfish.spartacus.application.db.master;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.pilotfish.spartacusclient.rest.json.Source;

@Slf4j
@Entity
@Getter
@Setter
public class VehicleModelValue extends GenericMasterValue {

    @ManyToOne
    @MapsId("masterRecordIdentifier")
    private VehicleModelMaster masterVehicleModel;

    public VehicleModelValue(){}

    public VehicleModelValue(String classification, String name, String value, Source source){
        super(classification, name, value, source);
    }

    public VehicleModelValue(String classification, String name, Integer value, Source source) {
        super(classification, name, value, source);
    }

    public VehicleModelValue(String classification, String name, Boolean value, Source source) {
        super(classification, name, value, source);
    }

    public VehicleModelValue(String classification, String name, Date value, Source source) {
        super(classification, name, value, source);
    }

    public VehicleModelValue(String classification, String name, String value,
        ValueType type, Source source) {
        super(classification, name, value, type, source);
    }

    @Override
    public boolean equals(Object other) {
        //Sonarcloud thinks that the master object reference might be important for equals.
        return super.equals(other);
    }

    @Override
    protected void remove() {
        this.masterVehicleModel = null;
    }

}
