package se.pilotfish.spartacus.service;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.auditing.AuditingHandler;
import se.pilotfish.spartacus.application.db.master.UserMaster;
import se.pilotfish.spartacus.repository.UserMasterRepository;
import se.pilotfish.spartacusclient.rest.json.CustomerReference;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.User;

@RunWith(MockitoJUnitRunner.class)
class UserMasterDataServiceImplTest {

  @Mock
  private AuditingHandler auditingHandler;

  @Mock
  private UserMasterRepository userMasterRepository;

  @InjectMocks
  UserMasterDataServiceImpl service;

  @BeforeEach
  public void initMocks() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void updateUserMaster() {
    User userEntity = User.builder()
        .customer(CustomerReference.builder().shortName("company1").build())
        .externalIdentifier("userIdentifier")
        .username("username")
        .build();
    Message message = Message.builder()
        .event(Event.CREATE_OR_UPDATE)
        .source(Source.CUSTOMER)
        .payload(userEntity)
        .build();

    UserMaster userMaster = UserMaster.builder().build();
    LocalDateTime now = LocalDateTime.now();
    userMaster.setCreated(now);
    userMaster.setUpdated(now);
    when(userMasterRepository.findByCustomerShortNameAndExternalIdentifier("company1", "userIdentifier")).thenReturn(
        Optional.of(userMaster));
    when(userMasterRepository.save(userMaster)).thenReturn(userMaster);

    service.updateUserMaster(message);

    verify(userMasterRepository).save(eq(userMaster));
  }

}