package se.pilotfish.spartacus.rest.service.v3;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import se.pilotfish.spartacus.rest.versions.Version_1_0;
import se.pilotfish.spartacusclient.rest.json.Customer;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Source;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WebMvcTest(value = CustomerController.class)
class CustomerControllerTest {

  @Inject
  protected MockMvc mvc;

  @Inject
  protected ObjectMapper objectMapper;

  @MockBean
  @Version_1_0
  EntityService entityService;

  @MockBean
  VersionHandler versionHandler;

  Customer customer;

  @BeforeEach
  public void setup() {
    customer = Customer.builder()
            .shortName("shortName")
            .build();

    when(versionHandler.getVersion("LATEST")).thenReturn(entityService);
  }

  @Test
  public void testUpdateByExternalIdNeedsAuthentication() throws Exception {
    ResultActions mvcResult = mvc.perform(
            post("/customers/v3/shortName")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(customer))
                .with(anonymous())
        )
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUpdateByExternalIdNeedsAdminRole() throws Exception {
    ResultActions mvcResult = mvc.perform(
            post("/customers/v3/shortName")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(customer))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_user")))
        )
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUpdateByExternalId() throws Exception {
    when(entityService.handleEvent(any(Event.class), any(Customer.class),
        any(String.class), any(Source.class), any()))
        .thenReturn(customer);

    ResultActions mvcResult = mvc.perform(
            post("/customers/v3/shortName")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(customer))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_administrator")))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.shortName").value(customer.getShortName()));

    verify(entityService).handleEvent(eq(Event.CREATE_OR_UPDATE), eq(customer),
        eq("LATEST"), eq(Source.AUTOMATIC_IMPORT), any());
  }

  @Test
  public void testCreate() throws Exception {
    when(entityService.handleEvent(any(Event.class), any(Customer.class),
        any(String.class), any(Source.class), any()))
        .thenReturn(customer);

    ResultActions mvcResult = mvc.perform(
            post("/customers/v3/")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(customer))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_administrator")))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.shortName").value(customer.getShortName()));

    verify(entityService).handleEvent(eq(Event.CREATE), eq(customer),
        eq("LATEST"), eq(Source.AUTOMATIC_IMPORT), any());
  }

  @Test
  public void testUpdate() throws Exception {
    when(entityService.handleEvent(any(Event.class), any(Customer.class),
        any(String.class), any(Source.class), any()))
        .thenReturn(customer);

    ResultActions mvcResult = mvc.perform(
            put("/customers/v3/shortName")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(customer))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_administrator")))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.shortName").value(customer.getShortName()));

    verify(entityService).handleEvent(eq(Event.UPDATE), eq(customer),
        eq("LATEST"), eq(Source.AUTOMATIC_IMPORT), any());
  }

}