package se.pilotfish.spartacus.kafka.producer;

import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import se.pilotfish.spartacusclient.rest.json.Message;

@Slf4j
@Component
public class InternalMessageProducerImpl implements InternalMessageProducer {

  private final KafkaTemplate<String, Message> kafkaTemplate;
  private final String internalTopic;

  @Inject
  public InternalMessageProducerImpl(
      KafkaTemplate<String, Message> kafkaTemplate, @Value("${kafka.topic.internal.name}") String internalTopic) {
    this.kafkaTemplate = kafkaTemplate;
    this.internalTopic = internalTopic;
  }

  @Override
  public void publish(Message message) {
    kafkaTemplate.send(internalTopic, message.messageKey(), message);
    log.debug("sent internal message {}", message);
  }

}
