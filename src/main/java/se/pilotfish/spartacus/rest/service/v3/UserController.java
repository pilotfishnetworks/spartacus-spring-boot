package se.pilotfish.spartacus.rest.service.v3;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.NotBlank;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.pilotfish.spartacus.application.failure.SpartacusFailureResult;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.User;

@RestController
@RequestMapping("users/v3")
@Tag(name = "users")
@ApiResponses(value = {
    @ApiResponse(responseCode = "403", description = "Unauthorized", content = @Content(schema = @Schema(implementation = SpartacusFailureResult.class))),
    @ApiResponse(responseCode = "422", description = "Invalid input", content = @Content(schema = @Schema(implementation = SpartacusFailureResult.class)))
})
public class UserController extends AbstractEntityController<User> {

  @Operation(summary = "Creates or updates a user. If the user has previously been added "+
      "to the same customer it will be updated otherwise created.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class)))})
  @PostMapping(value = "/{shortName}/by-external-id/{externalIdentifier}",
      consumes = {"application/json"},
      produces = {"application/json;charset=utf-8"}
  )
  @RolesAllowed("administrator")
  public User updateEntityByExternalId(
      @RequestBody User toSync,
      @Parameter(description = "customer group name", required = true) @PathVariable("shortName") @NotBlank String shortName,
      @Parameter(description = "id assigned this object by the customer", required = true) @PathVariable("externalIdentifier") @NotBlank String externalIdentifier,
      @Parameter(description = "version of the api") @RequestParam(name = "api-version", defaultValue = "LATEST") String version,
      @Parameter(description = "originator of the update") @RequestParam(name = "source", defaultValue = "AUTOMATIC_IMPORT") Source source,
      JwtAuthenticationToken authorization) {
    toSync.getCustomer().setShortName(shortName);
    toSync.setExternalIdentifier(externalIdentifier);
    return super.handleEvent(Event.CREATE_OR_UPDATE, toSync, version, source, authorization);
  }

  @Operation(summary = "Creates a user.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class)))})
  @PostMapping(value = "/{shortName}",
      consumes = {"application/json"},
      produces = {"application/json;charset=utf-8"}
  )
  @RolesAllowed("administrator")
  public User createEntity(
      @RequestBody User toSync,
      @Parameter(description = "customer group name", required = true) @PathVariable("shortName") @NotBlank String shortName,
      @Parameter(description = "version of the api") @RequestParam(name = "api-version", defaultValue = "LATEST") String version,
      @Parameter(description = "originator of the update") @RequestParam(name = "source", defaultValue = "AUTOMATIC_IMPORT") Source source,
      JwtAuthenticationToken authorization) {
    toSync.getCustomer().setShortName(shortName);
    return super.handleEvent(Event.CREATE, toSync, version, source, authorization);
  }

  @Operation(summary = "Updates a user.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class)))})
  @PutMapping(value = "/{shortName}/by-id/{masterId}",
      consumes = {"application/json"},
      produces = {"application/json;charset=utf-8"}
  )
  @RolesAllowed("administrator")
  public User updateEntityById(
      @RequestBody User toSync,
      @Parameter(description = "customer group name", required = true) @PathVariable("shortName") @NotBlank String shortName,
      @Parameter(description = "id assigned this object", required = true) @PathVariable("masterId") UUID masterId,
      @Parameter(description = "version of the api") @RequestParam(name = "api-version", defaultValue = "LATEST") String version,
      @Parameter(description = "originator of the update") @RequestParam(name = "source", defaultValue = "AUTOMATIC_IMPORT") Source source,
      JwtAuthenticationToken authorization) {
    toSync.getCustomer().setShortName(shortName);
    toSync.setMasterId(masterId);
    return super.handleEvent(Event.UPDATE, toSync, version, source, authorization);
  }

}
