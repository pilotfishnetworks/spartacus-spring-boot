package se.pilotfish.spartacus.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import se.pilotfish.spartacus.conf.topic.InternalTopicProperties;
import se.pilotfish.spartacus.conf.topic.MasterDataTopicProperties;
import se.pilotfish.spartacusclient.rest.json.Message;

@Configuration
@ConditionalOnProperty(value = "kafka.enabled", matchIfMissing = true)
@EnableKafka
@EnableConfigurationProperties({KafkaProperties.class, InternalTopicProperties.class, MasterDataTopicProperties.class})
public class KafkaConfig {

  @Inject
  private KafkaProperties kafkaProperties;

  @Inject
  private InternalTopicProperties internalTopicProperties;

  @Inject
  private MasterDataTopicProperties masterDataTopicProperties;

  @Inject
  private ObjectMapper objectMapper;

  @Bean
  public Map<String, Object> consumerConfigs() {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
    props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.getConsumer().getGroupId());
    return props;
  }

  @Bean
  public Map<String, Object> producerConfigs() {
    Map<String, Object> props = new HashMap<>();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
    return props;
  }

  @Bean
  public ConsumerFactory<String, Message> consumerFactory() {
    JsonDeserializer<Message> jsonDeserializer = new JsonDeserializer<>(objectMapper);
    jsonDeserializer.addTrustedPackages(Message.class.getPackage().getName());
    return new DefaultKafkaConsumerFactory<>(consumerConfigs(),
        new StringDeserializer(),
        jsonDeserializer);
  }

  @Bean
  public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, Message>> listenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, Message> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    factory.setReplyTemplate(kafkaTemplate());
    return factory;
  }

  @Bean
  public ProducerFactory<String, Message> producerFactory() {
    JsonSerializer<Message> jsonSerializer = new JsonSerializer<>(objectMapper);
    return new DefaultKafkaProducerFactory<>(producerConfigs(),
        new StringSerializer(),
        jsonSerializer);
  }

  @Bean
  public KafkaTemplate<String, Message> kafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }

  @Bean
  public KafkaAdmin admin() {
    Map<String, Object> configs = new HashMap<>();
    configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
    return new KafkaAdmin(configs);
  }

  @Bean
  public NewTopic internalTopic() {
    Map<String, String> configs = new HashMap<>();
    configs.put("retention.ms", internalTopicProperties.getRetention().toString());
    return new NewTopic(
        internalTopicProperties.getName(),
        internalTopicProperties.getPartitions(),
        internalTopicProperties.getReplication()
    ).configs(configs);
  }

  @Bean
  public NewTopic masterDataTopic() {
    Map<String, String> configs = new HashMap<>();
    configs.put("retention.ms", masterDataTopicProperties.getRetention().toString());
    return new NewTopic(
        masterDataTopicProperties.getName(),
        masterDataTopicProperties.getPartitions(),
        masterDataTopicProperties.getReplication()
    ).configs(configs);
  }
}
