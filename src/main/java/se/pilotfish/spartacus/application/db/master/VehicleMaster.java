package se.pilotfish.spartacus.application.db.master;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by magnusl on 2018-05-09.
 */
@Entity
@Table(uniqueConstraints =
        @UniqueConstraint(name = "VehicleMaster_customerShortName_externalIdentifier_idx",
                          columnNames = {"customerShortName", "externalIdentifier"}))
@Getter
@Setter
@NoArgsConstructor
public class VehicleMaster extends GenericMasterRecord {

    @NotNull
    private String customerShortName;

    private String externalIdentifier;

    @OneToMany(mappedBy = "masterVehicle", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @MapKey
    private Map<GenericMasterValueId, VehicleValue> vehicleValues = new HashMap<>();

    @Builder
    public VehicleMaster(UUID identifier, @NotNull String customerShortName, String externalIdentifier, Boolean hidden) {
        super(identifier, hidden);
        this.customerShortName = customerShortName;
        this.externalIdentifier = externalIdentifier;
    }

    @Override
    public Map<GenericMasterValueId, ? extends GenericMasterValue> getValues() {
        return vehicleValues;
    }

    @Override
    public void addValue(GenericMasterValue updatedValue) {
        VehicleValue updatedVehicleValue = (VehicleValue) updatedValue;
        updatedVehicleValue.setMasterVehicle(this);
        vehicleValues.put(updatedValue.getIdentifier(), updatedVehicleValue);
    }

    @Override
    public void removeValue(GenericMasterValue removedValue) {
        VehicleValue removedVehicleValue =  vehicleValues.remove(removedValue.getIdentifier());
        if (removedVehicleValue != null) {
            removedVehicleValue.remove();
        }
    }

}
