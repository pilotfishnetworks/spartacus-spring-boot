package se.pilotfish.spartacus.service;

import se.pilotfish.spartacus.application.failure.ValidationException;
import se.pilotfish.spartacusclient.rest.json.Entity;

public interface ValidationService {

  void validate(Entity entity) throws ValidationException;
}
