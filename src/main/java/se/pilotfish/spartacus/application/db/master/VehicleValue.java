package se.pilotfish.spartacus.application.db.master;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.pilotfish.spartacusclient.rest.json.Source;

/**
 * Created by magnusl on 2018-05-09.
 */
@Slf4j
@Entity
@Getter
@Setter
public class VehicleValue extends GenericMasterValue {

    @ManyToOne
    @MapsId("masterRecordIdentifier")
    private VehicleMaster masterVehicle;

    public VehicleValue(){}

    public VehicleValue(String classification, String name, String value, Source source){
        super(classification, name, value, source);
    }

    public VehicleValue(String classification, String name, Integer value, Source source) {
        super(classification, name, value, source);
    }

    public VehicleValue(String classification, String name, Boolean value, Source source) {
        super(classification, name, value, source);
    }

    public VehicleValue(String classification, String name, Date value, Source source) {
        super(classification, name, value, source);
    }

    public VehicleValue(String classification, String name, String value,
        ValueType type, Source source) {
        super(classification, name, value, type, source);
    }

    @Override
    public boolean equals(Object other) {
        //Sonarcloud thinks that the master object reference might be important for equals.
        return super.equals(other);
    }

    @Override
    protected void remove() {
        this.masterVehicle = null;
    }

}
