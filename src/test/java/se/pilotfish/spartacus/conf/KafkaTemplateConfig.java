package se.pilotfish.spartacus.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import se.pilotfish.spartacusclient.rest.json.Message;

public class KafkaTemplateConfig {

  @Autowired
  private KafkaConfig kafkaConfig;

  @Autowired
  private ObjectMapper mapper;

  @Bean
  public KafkaTemplate<String, Message> testKafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }

  @Bean
  public ProducerFactory<String, Message> producerFactory() {
    JsonSerializer<Message> jsonSerializer = new JsonSerializer<>(mapper);
    return new DefaultKafkaProducerFactory<>(kafkaConfig.producerConfigs(),
        new StringSerializer(),
        jsonSerializer);
  }


}
