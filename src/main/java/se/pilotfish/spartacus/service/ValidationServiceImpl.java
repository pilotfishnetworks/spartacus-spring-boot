package se.pilotfish.spartacus.service;

import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.failure.ErrorId;
import se.pilotfish.spartacus.application.failure.ValidationException;
import se.pilotfish.spartacus.application.validation.ValidationError;
import se.pilotfish.spartacus.application.validation.ValidatorEntity;
import se.pilotfish.spartacus.repository.ValidatorRepository;
import se.pilotfish.spartacusclient.rest.json.Entity;
import se.pilotfish.spartacusclient.rest.json.Entity.EntityType;

@Service
public class ValidationServiceImpl implements ValidationService {

  @Inject
  private ValidatorRepository validatorRepository;

  /**
   * Utility method that REST endpoints can use to validate an entity.
   * @param entity
   * @throws ValidationException
   */
  @Override
  public void validate(Entity entity) throws ValidationException {

    List<ValidationError> errors = new LinkedList<>();
    errors.addAll(performValidation(entity));

    if(!errors.isEmpty()){
      throw new ValidationException(ErrorId.VALIDATION_FAILED,
          "validation of " + entity.getEntityType() + " failed", errors);
    }

  }

  private List<ValidationError> performValidation(Entity entity) {

    String validatesFor = entity.getEntityType().equals(EntityType.CUSTOMER) ?
        "pilotfish" :
        entity.customerShortname();
    List<ValidatorEntity> validators = validatorRepository.findByValidatesForAndValidates(validatesFor,
        entity.getEntityType());

    List<ValidationError> errors = new LinkedList<>();
    for (ValidatorEntity validator : validators) {
      ValidationError failed = validator.validate(entity);
      if(failed != null) {
        errors.add(failed);
      }
    }
    return errors;

  }

}
