package se.pilotfish.spartacus.application.failure;

import org.springframework.http.HttpStatus;

public class AccessException extends SpartacusRuntimeException {

  public AccessException(String message) {
    super(ErrorId.ACCESS_TO_ENTITY_DENIED, message, null);
  }

  public AccessException(ErrorId id, String message, Throwable cause) {
    super(id, message, cause);
  }

  @Override
  public int getStatusCode(){
    return HttpStatus.FORBIDDEN.value();
  }
}
