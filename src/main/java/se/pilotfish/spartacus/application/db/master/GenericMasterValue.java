package se.pilotfish.spartacus.application.db.master;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import se.pilotfish.spartacusclient.rest.json.Source;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Slf4j
public abstract class GenericMasterValue {

    public static final String BASIC_CLASSIFICATION = "basic";
    public static final String IDENTIFIER_CLASSIFICATION = "identifier";
    public static final String PROPERTY_CLASSIFICATION = "property";

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @EmbeddedId
    private GenericMasterValueId identifier;

    private String value;

    @Enumerated(EnumType.STRING)
    private ValueType valueType;

    @Enumerated(EnumType.STRING)
    private Source source;

    @Column(name = "created", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @LastModifiedDate
    private LocalDateTime updated;

    @Column(name = "version", columnDefinition = "smallint default 0")
    @Version
    private short version;

    public GenericMasterValue(){}

    public GenericMasterValue(String classification, String name, String value, Source source){
        this.identifier = new GenericMasterValueId();
        this.identifier.setClassification(classification);
        this.identifier.setName(name);
        this.valueType = ValueType.STRING;
        this.value = value;
        this.source = source;
    }

    public GenericMasterValue(String classification, String name, Integer value, Source source) {
        this.identifier = new GenericMasterValueId();
        this.identifier.setClassification(classification);
        this.identifier.setName(name);
        this.valueType = ValueType.INTEGER;
        if(value != null) {
            this.value = String.valueOf(value);
        }
        this.source = source;
    }

    public GenericMasterValue(String classification, String name, Long value, Source source) {
        this.identifier = new GenericMasterValueId();
        this.identifier.setClassification(classification);
        this.identifier.setName(name);
        this.valueType = ValueType.LONG;
        if(value != null) {
            this.value = String.valueOf(value);
        }
        this.source = source;
    }

    public GenericMasterValue(String classification, String name, Boolean value, Source source) {
        this.identifier = new GenericMasterValueId();
        this.identifier.setClassification(classification);
        this.identifier.setName(name);
        this.valueType = ValueType.BOOLEAN;
        if(value != null) {
            this.value = String.valueOf(value);
        }
        this.source = source;
    }

    public GenericMasterValue(String classification, String name, Date value, Source source) {
        this.identifier = new GenericMasterValueId();
        this.identifier.setClassification(classification);
        this.identifier.setName(name);
        this.valueType = ValueType.DATE;
        if(value != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            this.value = format.format(value);
        }
        this.source = source;
    }

    public GenericMasterValue(String classification, String name, String value,
        ValueType type,
        Source source){
        this.identifier = new GenericMasterValueId();
        this.identifier.setClassification(classification);
        this.identifier.setName(name);
        this.valueType = type;
        this.value = value;
        this.source = source;
    }

    public String getClassification() {
        return getIdentifier().getClassification();
    }

    public String getName() {
        return getIdentifier().getName();
    }

    public boolean canBeRemovedBy(Source source) {
        return this.source.deleteWhenMissingFrom(source);
    }

    protected abstract void remove();

    public LocalDateTime dateValue(){
        try {
            if (getValueType() == ValueType.DATE) {
                return LocalDateTime.parse(getValue(), DATE_TIME_FORMATTER);
            }
        } catch (DateTimeParseException exc){
            log.warn("{} caused {}", getValue(), exc.getMessage());
        }
        throw new UnsupportedOperationException("Cannot convert none DATE type to date");

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GenericMasterValue that = (GenericMasterValue) o;
        return identifier.equals(that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
            "classification=" + identifier.getClassification() +
            ", name='" + identifier.getName() + '\'' +
            ", value='" + value + '\'' +
            ", type='" + valueType + '\'' +
            '}';
    }

    public boolean isNull() {
        return value == null;
    }

}
