package se.pilotfish.spartacus.application.db.master;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericMasterValueId implements Serializable {

  @NotNull
  private UUID masterRecordIdentifier;
  @NotNull
  private String classification;
  @NotNull
  private String name;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GenericMasterValueId that = (GenericMasterValueId) o;
    return classification.equals(that.classification) && name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(classification, name);
  }
}
