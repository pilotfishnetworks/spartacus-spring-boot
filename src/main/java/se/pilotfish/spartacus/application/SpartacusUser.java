package se.pilotfish.spartacus.application;

import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import se.pilotfish.keycloak.TokenParser;
import se.pilotfish.spartacusclient.rest.json.Entity;

/**
 * Created by magnusl on 2017-02-16.
 */
public class SpartacusUser {

    public static final Logger logger = LogManager.getLogger();
    private Jwt jwt;
    protected TokenParser parser;
    private JwtAuthenticationToken authorization;

    public SpartacusUser(JwtAuthenticationToken authorization){
        this.authorization = authorization;
        this.jwt = authorization.getToken();
        if(this.jwt == null){
            logger.warn("No jwt set for user {}", authorization.getName());
        } else {
            parser = new TokenParser();
            parser.parseJwt(this.jwt);
        }

    }

    public String getUserName(){
        return authorization.getName();
    }

    /**
     *
     * @return short name for user company or "default" if not set.
     */
    public String getCompanyShortName() {

        String result = null;
        if(jwt != null) {

            Map<String, Object> extrasClaim = jwt.getClaimAsMap("wanda_extras");
            if (extrasClaim != null) { //.isNull does not work... json value says - isNull==true
                result = (String) extrasClaim.get("company_shortname");
            }

        }

        if(result != null){
            return result;
        }

        logger.warn("Missing company shortname for {} keycloak upgrade or reconfigure company.", authorization.getName());
        return "default";

    }

    public void setJwt(Jwt jwt){
        this.jwt = jwt;
    }

    public String synchronizingCaller(){
        String shortName = getCompanyShortName();
        //For backwards compatibility we fallback to user name.
        if ("default".equals(shortName)) {
            return getUserName();
        } else {
            return shortName;
        }
    }

    /**
     * Will check if the user has access to an entity. Access is granted if the
     * user is administrator for the company that the entity belongs to. If access
     * is NOT granted an AccessException will be thrown.
     *
     * @param entity object the user wants to interact with
     * @return true if access is granted.
     */
    public boolean canUpdate(Entity entity) {
        // TODO: Implement based on externalIds of companies
//        if (! parser.hasRoleAndCompanyAccess("administrator", entity.belongsToId())) {
//            throw new AccessException("No access to " + entity.getEntityType());
//        }
        return true;
    }

    public boolean canAccessShortName(String shortname){
        return shortname.equals(getCompanyShortName());
    }
}
