package se.pilotfish.spartacus.conf.topic;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "kafka.topic.internal")
public class InternalTopicProperties extends TopicProperties{
}
