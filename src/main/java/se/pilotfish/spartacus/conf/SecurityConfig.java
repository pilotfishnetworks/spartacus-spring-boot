package se.pilotfish.spartacus.conf;

import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import com.nimbusds.jwt.proc.JWTClaimsSetAwareJWSKeySelector;
import com.nimbusds.jwt.proc.JWTProcessor;
import java.util.Collections;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.boot.actuate.audit.InMemoryAuditEventRepository;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import se.pilotfish.keycloak.TokenParser;

@Configuration
@EnableGlobalMethodSecurity(
    prePostEnabled = true,
    securedEnabled = true,
    jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String AUTHORITIES_CLAIM_NAME = "authorities";

  // Set qequired to false, to allow tests to execute without these components available.
  @Autowired(required = false)
  private JWTClaimsSetAwareJWSKeySelector<SecurityContext> jwsKeySelector;
  @Autowired(required = false)
  private OAuth2TokenValidator<Jwt> jwsIssuerValidator;

  @Bean
  public AuditEventRepository auditEventRepository() {
    return new InMemoryAuditEventRepository();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors()
        .and()
          .authorizeRequests()
            .antMatchers("/docs/v3/api-docs/**","/docs/v3/api-docs.yaml",
                    "/docs/swagger-ui/**","/docs/swagger-ui.html").permitAll()  // whitelist Swagger UI resources
            .antMatchers("/actuator/**").permitAll()  // whitelist Actuator resources
            .antMatchers("/**").authenticated()  // require authentication for any endpoint that's not whitelisted
        .and()
          .oauth2ResourceServer()
            .jwt();
  }

  @Bean
  public JWTProcessor jwtProcessor() {
    ConfigurableJWTProcessor<SecurityContext> jwtProcessor =
        new DefaultJWTProcessor();
    jwtProcessor.setJWTClaimsSetAwareJWSKeySelector(jwsKeySelector);
    return jwtProcessor;
  }

  @Bean
  public JwtDecoder jwtDecoder(JWTProcessor jwtProcessor, OAuth2ResourceServerProperties properties) {
    NimbusJwtDecoder jwtDecoder = new NimbusJwtDecoder(jwtProcessor);
    OAuth2TokenValidator<Jwt> validator = new DelegatingOAuth2TokenValidator<>
        (JwtValidators.createDefault(), jwsIssuerValidator);
    jwtDecoder.setJwtValidator(validator);
    jwtDecoder.setClaimSetConverter(new WandaRolesClaimAdapter());
    return jwtDecoder;
  }

  @Bean
  public JwtAuthenticationConverter jwtAuthenticationConverter() {
    JwtGrantedAuthoritiesConverter authoritiesConverter = new JwtGrantedAuthoritiesConverter();
    authoritiesConverter.setAuthorityPrefix("ROLE_");
    authoritiesConverter.setAuthoritiesClaimName(AUTHORITIES_CLAIM_NAME);

    JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
    converter.setJwtGrantedAuthoritiesConverter(authoritiesConverter);
    return converter;
  }

  public static class WandaRolesClaimAdapter implements
      Converter<Map<String, Object>, Map<String, Object>> {

    private final MappedJwtClaimSetConverter delegate =
        MappedJwtClaimSetConverter.withDefaults(Collections.emptyMap());

    public Map<String, Object> convert(Map<String, Object> claims) {
      Map<String, Object> convertedClaims = this.delegate.convert(claims);
      @SuppressWarnings("unchecked")
      Map<String, Object> wandaRolesClaim = (Map<String, Object>) convertedClaims.get(TokenParser.ROLES_KEY);
      if (wandaRolesClaim != null) {
        String authorities = String.join(" ", wandaRolesClaim.keySet());
        convertedClaims.put(AUTHORITIES_CLAIM_NAME, authorities);
      }
      return convertedClaims;
    }
  }
}