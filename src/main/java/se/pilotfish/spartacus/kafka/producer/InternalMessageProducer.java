package se.pilotfish.spartacus.kafka.producer;

import se.pilotfish.spartacusclient.rest.json.Message;

public interface InternalMessageProducer {

  void publish(Message message);
}
