package se.pilotfish.spartacus.application.validation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("not-null")
public class NotNullValidator extends ValidatorEntity {

  @Override
  public boolean isValid(se.pilotfish.spartacusclient.rest.json.Entity entity) {
    return fetchProperty(entity) != null;
  }

  @Override
  public ValidationError validate(se.pilotfish.spartacusclient.rest.json.Entity entity) {
    if(!isValid(entity)) {
      return generateValidationError();
    }
    return null;

  }

}
