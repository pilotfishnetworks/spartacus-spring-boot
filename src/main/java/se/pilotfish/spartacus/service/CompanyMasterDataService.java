package se.pilotfish.spartacus.service;

import se.pilotfish.spartacusclient.rest.json.Message;

public interface CompanyMasterDataService {

  Message updateCompanyMaster(Message message);

}
