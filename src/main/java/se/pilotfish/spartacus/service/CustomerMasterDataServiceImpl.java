package se.pilotfish.spartacus.service;

import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.BASIC_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.PROPERTY_CLASSIFICATION;

import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.db.master.CustomerMaster;
import se.pilotfish.spartacus.application.db.master.CustomerValue;
import se.pilotfish.spartacus.application.db.master.ValueType;
import se.pilotfish.spartacus.application.failure.EntityGoneException;
import se.pilotfish.spartacus.repository.CustomerMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Customer;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;

@Slf4j
@Service
public class CustomerMasterDataServiceImpl implements CustomerMasterDataService {

  @Inject
  private AuditingHandler auditingHandler;

  @Inject
  private CustomerMasterRepository customerMasterRepository;

  @Override
  @Transactional
  public Message updateCustomerMaster(Message message) {
    Customer customer = (Customer) message.getPayload();
    CustomerMaster customerMaster = null;
    switch (message.getEvent()) {
      case CREATE:
        customerMaster = createCustomerMaster(customer);
        break;
      case UPDATE:
        customerMaster = customerMasterRepository.findByShortName(customer.getShortName())
            .orElseThrow(() -> new EntityGoneException("No such Company: "+ customer.getShortName(), null));
        break;
      case CREATE_OR_UPDATE:
        message.setEvent(Event.UPDATE);
        customerMaster = customerMasterRepository.findByShortName(customer.getShortName())
            .orElseGet(() -> {
              message.setEvent(Event.CREATE);
              return createCustomerMaster(customer);
            });
        break;
      default:
        log.error("Unsupported event type {}", message.getEvent());
        return message;
    }
    Set<CustomerValue> updatedValues = asCustomerValues(customer, message.getSource());
    customerMaster.updateFrom(updatedValues);
    // Mark customerMaster as updated, even though only its related values may have changed
    auditingHandler.markModified(customerMaster);
    customerMaster = customerMasterRepository.save(customerMaster);
    customer.getMeta().setCreated(customerMaster.getCreated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    customer.getMeta().setUpdated(customerMaster.getUpdated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    return message;
  }

  private CustomerMaster createCustomerMaster(Customer customer) {
    return CustomerMaster.builder()
        .shortName(customer.customerShortname())
        .build();
  }

  private Set<CustomerValue> asCustomerValues(Customer updatedCustomer, Source source) {
    Set<CustomerValue> updatedValues = new HashSet<>();
    updatedValues.add(new CustomerValue(BASIC_CLASSIFICATION, "name", updatedCustomer.getShortName(), source));
    if (updatedCustomer.getState() != null) {
      updatedValues.add(new CustomerValue(BASIC_CLASSIFICATION, "state", updatedCustomer.getState().toString(),
          source));
    }
    if (updatedCustomer.getProperties() != null) {
      updatedCustomer.getProperties().forEach(p ->
          updatedValues.add(new CustomerValue(PROPERTY_CLASSIFICATION, p.getName(), p.getValue(), ValueType.valueOf(p.getType()),
              source)));
    }
    if(updatedCustomer.getFeatures() != null) {
      for (int i = 0; i < updatedCustomer.getFeatures().size(); i++) {
        updatedValues.add(new CustomerValue(BASIC_CLASSIFICATION, "feature_size",
            Integer.valueOf(updatedCustomer.getFeatures().size()), source));
        String featureName = updatedCustomer.getFeatures().get(i).getName();
        updatedValues.add(new CustomerValue(BASIC_CLASSIFICATION, "feature_" + i,
            featureName, source));
      }
    }
    return updatedValues;
  }


}
