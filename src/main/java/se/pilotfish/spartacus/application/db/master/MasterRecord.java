package se.pilotfish.spartacus.application.db.master;

public interface MasterRecord {
    void setHidden(Boolean hidden);
}
