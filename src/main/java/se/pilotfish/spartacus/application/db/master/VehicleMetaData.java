package se.pilotfish.spartacus.application.db.master;

import java.util.Map;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by magnusl on 2019-02-07.
 */
@Entity
@Table(indexes = {
        @Index(name="Caller",
                columnList = "caller")})
@Getter
@Setter
public class VehicleMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long identifier;
    private String caller;

    @ElementCollection
    private Map<String, String> configuration;

}
