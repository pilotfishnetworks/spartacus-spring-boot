package se.pilotfish.spartacus.service;

import se.pilotfish.spartacusclient.rest.json.Message;

public interface VehicleMasterDataService {

  Message updateVehicleMaster(Message message);

}
