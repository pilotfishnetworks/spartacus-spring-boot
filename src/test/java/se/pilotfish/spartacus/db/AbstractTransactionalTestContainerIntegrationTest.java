package se.pilotfish.spartacus.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public abstract class AbstractTransactionalTestContainerIntegrationTest extends AbstractTestContainerIntegrationTest {

    @Autowired
    private PlatformTransactionManager txManager;

    private TransactionStatus transaction;

    public void beginTransaction() {
        transaction = txManager.getTransaction(new DefaultTransactionDefinition());
    }

    public void rollback() {
        txManager.rollback(transaction);
        transaction = null;
    }

    public void commit() {
        txManager.commit(transaction);
        transaction = null;
    }

}
