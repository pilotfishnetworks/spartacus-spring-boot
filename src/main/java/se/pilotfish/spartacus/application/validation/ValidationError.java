package se.pilotfish.spartacus.application.validation;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class ValidationError implements Serializable {

  @Getter
  @Setter
  String property;

  @Getter
  @Setter
  String description;

}
