package se.pilotfish.spartacus.rest.service.v3;

import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import se.pilotfish.spartacusclient.rest.json.Entity;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Source;

public interface EntityService {

  /**
   * Handle an entity event.
   *
   * @param event The event type
   * @param toSync data for new entity
   * @param version version of the api
   * @param source originator of the update

   * @return the created entity
   */
  Entity handleEvent(Event event, Entity toSync, String version, Source source, JwtAuthenticationToken authorization);

}
