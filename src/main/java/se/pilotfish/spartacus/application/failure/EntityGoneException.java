package se.pilotfish.spartacus.application.failure;

import org.springframework.http.HttpStatus;

public class EntityGoneException extends SpartacusRuntimeException {

    public EntityGoneException(String message, Throwable cause){
        super(ErrorId.EXTERNAL_ID_MISSING, message, cause);
    }

    @Override
    public int getStatusCode(){
        return HttpStatus.GONE.value();
    }
}
