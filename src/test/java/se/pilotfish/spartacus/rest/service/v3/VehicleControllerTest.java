package se.pilotfish.spartacus.rest.service.v3;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import javax.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import se.pilotfish.spartacus.rest.versions.Version_1_0;
import se.pilotfish.spartacus.util.UUIDUtil;
import se.pilotfish.spartacusclient.rest.json.CustomerReference;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.Vehicle;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WebMvcTest(value = VehicleController.class)
class VehicleControllerTest {

  @Inject
  protected MockMvc mvc;

  @Inject
  protected ObjectMapper objectMapper;

  @MockBean
  @Version_1_0
  EntityService entityService;

  @MockBean
  VersionHandler versionHandler;

  Vehicle vehicle;

  @BeforeEach
  public void setup() {
    vehicle = Vehicle.builder()
        .masterId(UUID.randomUUID())
        .customer(CustomerReference.builder().shortName("shortName").build())
        .externalIdentifier("externalIdentifier")
        .vehicleNumber("vehicleNumber")
        .build();

    when(versionHandler.getVersion("LATEST")).thenReturn(entityService);
  }

  @Test
  public void testUpdateByExternalIdNeedsAuthentication() throws Exception {
    ResultActions mvcResult = mvc.perform(
            post("/vehicles/v3/shortName/by-external-id/externalIdentifier")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(vehicle))
                .with(anonymous())
        )
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUpdateByExternalIdNeedsAdminRole() throws Exception {
    ResultActions mvcResult = mvc.perform(
            post("/vehicles/v3/shortName/by-external-id/externalIdentifier")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(vehicle))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_user")))
        )
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUpdateByExternalId() throws Exception {
    when(entityService.handleEvent(any(Event.class), any(Vehicle.class),
        any(String.class), any(Source.class), any()))
        .thenReturn(vehicle);

    ResultActions mvcResult = mvc.perform(
            post("/vehicles/v3/shortName/by-external-id/externalIdentifier")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(vehicle))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_administrator")))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.masterId").value(vehicle.getIdentifier()))
        .andExpect(jsonPath("$.externalIdentifier").value(vehicle.getExternalIdentifier()))
        .andExpect(jsonPath("$.vehicleNumber").value(vehicle.getVehicleNumber()));

    verify(entityService).handleEvent(eq(Event.CREATE_OR_UPDATE), eq(vehicle),
        eq("LATEST"), eq(Source.AUTOMATIC_IMPORT), any());
  }

  @Test
  public void testCreate() throws Exception {
    when(entityService.handleEvent(any(Event.class), any(Vehicle.class),
        any(String.class), any(Source.class), any()))
        .thenReturn(vehicle);

    ResultActions mvcResult = mvc.perform(
            post("/vehicles/v3/shortName")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(vehicle))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_administrator")))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.masterId").value(vehicle.getIdentifier()))
        .andExpect(jsonPath("$.externalIdentifier").value(vehicle.getExternalIdentifier()))
        .andExpect(jsonPath("$.vehicleNumber").value(vehicle.getVehicleNumber()));

    verify(entityService).handleEvent(eq(Event.CREATE), eq(vehicle),
        eq("LATEST"), eq(Source.AUTOMATIC_IMPORT), any());
  }

  @Test
  public void testUpdateById() throws Exception {
    vehicle.setMasterId(UUIDUtil.toUUID(1));
    when(entityService.handleEvent(any(Event.class), any(Vehicle.class),
        any(String.class), any(Source.class), any()))
        .thenReturn(vehicle);

    ResultActions mvcResult = mvc.perform(
            put("/vehicles/v3/shortName/by-id/000000000000-0000-0000-0000-00000001")
                .contentType("application/json")
                .accept("application/json")
                .content(objectMapper.writeValueAsString(vehicle))
                .with(jwt().authorities(new SimpleGrantedAuthority("ROLE_administrator")))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.masterId").value(vehicle.getIdentifier()))
        .andExpect(jsonPath("$.externalIdentifier").value(vehicle.getExternalIdentifier()))
        .andExpect(jsonPath("$.vehicleNumber").value(vehicle.getVehicleNumber()));

    verify(entityService).handleEvent(eq(Event.UPDATE), eq(vehicle),
        eq("LATEST"), eq(Source.AUTOMATIC_IMPORT), any());
  }

}