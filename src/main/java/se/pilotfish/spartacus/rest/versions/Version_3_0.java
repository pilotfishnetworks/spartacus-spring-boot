package se.pilotfish.spartacus.rest.versions;

import java.lang.annotation.RetentionPolicy;
import javax.inject.Qualifier;

@java.lang.annotation.Documented
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Version_3_0 {

}
