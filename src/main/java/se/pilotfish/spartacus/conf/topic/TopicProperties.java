package se.pilotfish.spartacus.conf.topic;

import lombok.Data;

@Data
public abstract class TopicProperties {

  private String name;
  private Integer partitions;
  private Short replication;
  private Long retention;

}
