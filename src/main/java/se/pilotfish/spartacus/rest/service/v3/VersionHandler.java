package se.pilotfish.spartacus.rest.service.v3;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("version-handler")
public class VersionHandler {

  private Map<String, EntityService> versions = new HashMap<>();

  public static final Logger logger = LogManager.getLogger();

  public void register(String version, EntityService service) {
    versions.put(version, service);
  }

  public Set<String> listApiVersions(){
    return versions.keySet();
  }

  public EntityService getVersion(String version){
    EntityService implementation = versions.get(version);
    if(implementation == null){
      logger.warn("Unsupported version of Service, falling back to LATEST");
      implementation = versions.get("LATEST");
    }
    return implementation;
  }

}
