package se.pilotfish.spartacus.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.pilotfish.spartacus.application.db.master.UserMaster;

@Repository
public interface UserMasterRepository extends CrudRepository<UserMaster, UUID> {

  Optional<UserMaster> findByCustomerShortNameAndExternalIdentifier(String customerShortName, String externalIdentifier);

}
