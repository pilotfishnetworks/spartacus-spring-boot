package se.pilotfish.spartacus.service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacusclient.rest.json.Message;

@Slf4j
@Service
public class MasterDataServiceImpl implements MasterDataService {

  @Inject
  private CompanyMasterDataService companyMasterDataService;

  @Inject
  private CustomerMasterDataService customerMasterDataService;

  @Inject
  private UserMasterDataService userMasterDataService;

  @Inject
  private VehicleMasterDataService vehicleMasterDataService;

  @Inject
  private VehicleModelMasterDataService vehicleModelMasterDataService;

  @Override
  @Transactional
  public Message updateMasterData(Message message) {
    switch(message.getPayloadType()) {
      case COMPANY:
        return companyMasterDataService.updateCompanyMaster(message);
      case CUSTOMER:
        return customerMasterDataService.updateCustomerMaster(message);
      case USER:
        return userMasterDataService.updateUserMaster(message);
      case VEHICLE:
        return vehicleMasterDataService.updateVehicleMaster(message);
      case VEHICLE_MODEL:
        return vehicleModelMasterDataService.updateVehicleModelMaster(message);
      default:
        log.error("Invalid message type {}", message.getPayloadType());
        return message;
    }
  }

}
