package se.pilotfish.spartacus.application.db.master;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(uniqueConstraints =
        @UniqueConstraint(name = "CustomerMaster_shortName_idx",
                          columnNames = {"shortName"}))
@Getter
@Setter
@NoArgsConstructor
public class CustomerMaster extends GenericMasterRecord {

  @NotNull
  private String shortName;

  @OneToMany(mappedBy = "masterCustomer", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @MapKey
  Map<GenericMasterValueId, CustomerValue> customerValues = new HashMap<>();

  @Builder
  public CustomerMaster(UUID identifier, @NotNull String shortName, Boolean hidden) {
    super(identifier, hidden);
    this.shortName = shortName;
  }

  @Override
  public Map<GenericMasterValueId, ? extends GenericMasterValue> getValues() {
    return customerValues;
  }

  @Override
  public void addValue(GenericMasterValue updatedValue) {
    CustomerValue updatedCustomerValue = (CustomerValue) updatedValue;
    updatedCustomerValue.setMasterCustomer(this);
    customerValues.put(updatedValue.getIdentifier(), updatedCustomerValue);
  }

  @Override
  public void removeValue(GenericMasterValue removedValue) {
    CustomerValue removedCustomerValue =  customerValues.remove(removedValue.getIdentifier());
    if (removedCustomerValue != null) {
      removedCustomerValue.remove();
    }
  }


}
