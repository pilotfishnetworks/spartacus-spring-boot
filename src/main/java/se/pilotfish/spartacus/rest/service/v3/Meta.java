package se.pilotfish.spartacus.rest.service.v3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Meta {

  private Integer majorVersion;

  private Integer minorVersion;

}
