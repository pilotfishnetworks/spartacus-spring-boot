package se.pilotfish.spartacus.kafka.consumer;

import javax.inject.Inject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.pilotfish.spartacus.kafka.producer.MasterDataMessageProducer;
import se.pilotfish.spartacus.service.MasterDataService;
import se.pilotfish.spartacusclient.rest.json.Message;

@Slf4j
@RequiredArgsConstructor
@Component
public class InternalMessageConsumer {

  @Inject
  private final MasterDataService masterDataService;
  @Inject
  private final MasterDataMessageProducer masterDataMessageProducer;

  @Transactional
  @KafkaListener(topics = "${kafka.topic.internal.name}", containerFactory = "listenerContainerFactory")
  public void receive(Message message) {
    log.debug("received internal message {} ", message);
    Message processedMessage = masterDataService.updateMasterData(message);
    masterDataMessageProducer.publish(processedMessage);
  }
}
