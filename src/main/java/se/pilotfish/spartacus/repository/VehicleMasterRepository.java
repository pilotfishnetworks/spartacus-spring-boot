package se.pilotfish.spartacus.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.pilotfish.spartacus.application.db.master.VehicleMaster;

@Repository
public interface VehicleMasterRepository extends CrudRepository<VehicleMaster, UUID> {

  Optional<VehicleMaster> findByCustomerShortNameAndExternalIdentifier(String customerShortName, String externalIdentifier);

}
