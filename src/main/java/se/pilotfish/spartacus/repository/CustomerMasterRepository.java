package se.pilotfish.spartacus.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.pilotfish.spartacus.application.db.master.CustomerMaster;

@Repository
public interface CustomerMasterRepository extends CrudRepository<CustomerMaster, UUID> {

  Optional<CustomerMaster> findByShortName(String shortName);

}
