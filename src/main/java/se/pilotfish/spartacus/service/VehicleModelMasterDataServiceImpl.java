package se.pilotfish.spartacus.service;

import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.BASIC_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.IDENTIFIER_CLASSIFICATION;
import static se.pilotfish.spartacus.application.db.master.GenericMasterValue.PROPERTY_CLASSIFICATION;

import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.stereotype.Service;
import se.pilotfish.spartacus.application.db.master.ValueType;
import se.pilotfish.spartacus.application.db.master.VehicleModelMaster;
import se.pilotfish.spartacus.application.db.master.VehicleModelValue;
import se.pilotfish.spartacus.application.failure.EntityGoneException;
import se.pilotfish.spartacus.repository.VehicleModelMasterRepository;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Source;
import se.pilotfish.spartacusclient.rest.json.VehicleModel;

@Slf4j
@Service
public class VehicleModelMasterDataServiceImpl implements VehicleModelMasterDataService {

  @Inject
  private AuditingHandler auditingHandler;

  @Inject
  private VehicleModelMasterRepository vehicleModelMasterRepository;

  @Override
  @Transactional
  public Message updateVehicleModelMaster(Message message) {
    VehicleModel vehicleModel = (VehicleModel) message.getPayload();
    VehicleModelMaster vehicleModelMaster = null;
    switch (message.getEvent()) {
      case CREATE:
        vehicleModelMaster = createVehicleModelMaster(vehicleModel);
        break;
      case UPDATE:
        vehicleModelMaster = vehicleModelMasterRepository.findById(vehicleModel.getMasterId())
            .orElseThrow(() -> new EntityGoneException("No such Vehicle: "+ vehicleModel.getMasterId(), null));
        break;
      case CREATE_OR_UPDATE:
        message.setEvent(Event.UPDATE);
        vehicleModelMaster = vehicleModelMasterRepository.findByCustomerShortNameAndExternalIdentifier(
                vehicleModel.customerShortname(), vehicleModel.getExternalIdentifier())
            .orElseGet(() -> {
              message.setEvent(Event.CREATE);
              return createVehicleModelMaster(vehicleModel);
            });
        break;
      default:
        log.error("Unsupported event type {}", message.getEvent());
        return message;
    }
    Set<VehicleModelValue> updatedValues = asVehicleValues(vehicleModel, message.getSource());
    vehicleModelMaster.updateFrom(updatedValues);
    // Mark vehicleModelMaster as updated, even though only its related values may have changed
    auditingHandler.markModified(vehicleModelMaster);
    vehicleModelMaster = vehicleModelMasterRepository.save(vehicleModelMaster);
    vehicleModel.getMeta().setCreated(vehicleModelMaster.getCreated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    vehicleModel.getMeta().setUpdated(vehicleModelMaster.getUpdated().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
    return message;
  }

  private VehicleModelMaster createVehicleModelMaster(VehicleModel vehicleModel) {
    return VehicleModelMaster.builder()
        .customerShortName(vehicleModel.customerShortname())
        .externalIdentifier(vehicleModel.getExternalIdentifier())
        .build();
  }

  private Set<VehicleModelValue> asVehicleValues(VehicleModel updatedVehicleModel, Source source) {
    Set<VehicleModelValue> updatedValues = new HashSet<>();
    updatedValues.add(new VehicleModelValue(IDENTIFIER_CLASSIFICATION, "externalIdentifier", updatedVehicleModel.getExternalIdentifier(), source));
    updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "vehicleType", updatedVehicleModel.getVehicleType(), source));
    updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "manufacturer", updatedVehicleModel.getManufacturer(), source));
    updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "model", updatedVehicleModel.getModel(), source));
    updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "year", updatedVehicleModel.getYear(), source));
    updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "engineModel", updatedVehicleModel.getEngineModel(), source));
    updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "engineType", updatedVehicleModel.getEngineType(), source));
    if (updatedVehicleModel.getState() != null) {
      updatedValues.add(new VehicleModelValue(BASIC_CLASSIFICATION, "state", updatedVehicleModel.getState().toString(),
          source));
    }
    if (updatedVehicleModel.getProperties() != null) {
      updatedVehicleModel.getProperties().forEach(p ->
          updatedValues.add(new VehicleModelValue(PROPERTY_CLASSIFICATION, p.getName(), p.getValue(), ValueType.valueOf(p.getType()),
              source)));
    }
    return updatedValues;
  }

}
