package se.pilotfish.spartacus.util;

import java.util.UUID;

public final class UUIDUtil {

    private static final String FORMAT = "00000000-0000-0000-0000-%012d";
    private UUIDUtil() {}

    public static UUID toUUID(int i) {
        return UUID.fromString(toUUIDString(i));
    }

    public static String toUUIDString(int i) {
        return String.format(FORMAT, i);
    }
}
