package se.pilotfish.spartacus.application.db.master;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import se.pilotfish.spartacus.util.UUIDGenerator;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@NoArgsConstructor
public abstract class GenericMasterRecord implements MasterRecord {

    @Id
    @Column(unique = true, nullable = false, updatable = false, columnDefinition = "BINARY(16)")
    public UUID identifier;

    private Boolean hidden;

    @Column(name = "created", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @LastModifiedDate
    private LocalDateTime updated;

    @Column(name = "version", columnDefinition = "smallint default 0")
    @Version
    private short version;

    public GenericMasterRecord(UUID identifier, Boolean hidden) {
        this.identifier = identifier != null ? identifier : UUIDGenerator.generateUUID();
        this.hidden = hidden;
    }

    public boolean isNew() {
        return updated == null;
    }

    public abstract Map<GenericMasterValueId, ? extends GenericMasterValue> getValues();

    public abstract void addValue(GenericMasterValue updatedValue);

    public abstract void removeValue(GenericMasterValue updatedValue);

    protected void addOrUpdateValue(GenericMasterValue updatedValue) {
        GenericMasterValue existing = getValues().get(updatedValue.getIdentifier());
        if (existing != null) {
            // Existing value
            if(updatedValue.isNull() && existing.canBeRemovedBy(updatedValue.getSource())){
                removeValue(existing);
            } else {
                // Update value if necessary
                if (!existing.getValue().equals(updatedValue.getValue())) {
                    existing.setValue(updatedValue.getValue());
                    existing.setSource(updatedValue.getSource());
                }
            }
        } else if(!updatedValue.isNull()) {
            addValue(updatedValue);
        } else {
            //Missing and is still null - do nothing.
        }
    }

    public void updateFrom(Collection<? extends GenericMasterValue> updatedValues) {
        // Add or update with new values
        updatedValues.forEach(this::addOrUpdateValue);
        // Remove values no longer present
        Iterator<? extends Entry<GenericMasterValueId, ? extends GenericMasterValue>> it = getValues().entrySet()
            .iterator();
        while (it.hasNext()) {
            Map.Entry<GenericMasterValueId, ? extends GenericMasterValue> entry = it.next();
            if (!updatedValues.contains(entry.getValue())) {
                entry.getValue().remove();
                it.remove();
            }
        }
    }
}
