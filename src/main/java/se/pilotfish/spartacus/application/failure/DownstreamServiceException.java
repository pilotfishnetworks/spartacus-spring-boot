package se.pilotfish.spartacus.application.failure;

/**
 * Signals internal problems in a downstream service. In contrast to problems with communicating with the service.
 */
public class DownstreamServiceException extends SpartacusRuntimeException {

    private final int downstreamStatus;

    public DownstreamServiceException(int downstreamStatus, ErrorId id, String message) {
        super(id, message, null);
        this.downstreamStatus = downstreamStatus;
    }

    @Override
    public int getStatusCode() {
        return downstreamStatus;
    }

}