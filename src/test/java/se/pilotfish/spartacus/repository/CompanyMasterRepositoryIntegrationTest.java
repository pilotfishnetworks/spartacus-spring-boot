package se.pilotfish.spartacus.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static se.pilotfish.spartacus.util.UUIDUtil.toUUID;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import java.time.Instant;
import java.util.Optional;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.DateTimeProvider;
import se.pilotfish.spartacus.annotation.SpringBootDbIntegrationTest;
import se.pilotfish.spartacus.application.db.master.CompanyMaster;
import se.pilotfish.spartacus.application.db.master.CompanyValue;
import se.pilotfish.spartacus.db.AbstractTestContainerIntegrationTest;
import se.pilotfish.spartacusclient.rest.json.Source;

@SpringBootDbIntegrationTest
class CompanyMasterRepositoryIntegrationTest extends AbstractTestContainerIntegrationTest {

  @MockBean
  private DateTimeProvider dateTimeProvider;

  @SpyBean
  private AuditingHandler handler;

  @Inject
  private CompanyMasterRepository repo;

  @BeforeEach
  @DataSet("datasets/CustomerMaster.yml")
  void setUp() throws Exception {
    handler.setDateTimeProvider(dateTimeProvider);
  }

  @AfterEach
  @DataSet("datasets/clean.yml")
  void teardown() {
  }

  @Test
  @DataSet("datasets/CompanyMaster.yml")
  void findByCustomerShortNameAndExternalIdentifier() {
    CompanyMaster companyMaster = repo.findByCustomerShortNameAndExternalIdentifier("company1", "externalIdentifier").orElse(null);
    assertThat(companyMaster).isNotNull();
    assertThat(companyMaster.getIdentifier()).isEqualTo(toUUID(1));
    assertThat(companyMaster.getCompanyValues().size()).isEqualTo(3);
  }

  @Test
  @DataSet("datasets/CompanyMaster.yml")
  void findById() {
    CompanyMaster companyMaster = repo.findById(toUUID(1)).orElse(null);
    assertThat(companyMaster).isNotNull();
  }

  @Test
  @ExpectedDataSet("datasets/CompanyMaster.yml")
  void createCompanyMaster() {
    CompanyMaster companyMaster = CompanyMaster.builder()
        .identifier(toUUID(1))
        .externalIdentifier("externalIdentifier")
        .customerShortName("company1")
        .hidden(false)
        .build();
    CompanyValue companyValue1 = new CompanyValue("basic", "name","company1", Source.AUTOMATIC_IMPORT);
    companyMaster.addValue(companyValue1);
    CompanyValue companyValue2 = new CompanyValue("basic", "type","type1", Source.AUTOMATIC_IMPORT);
    companyMaster.addValue(companyValue2);
    CompanyValue companyValue3 = new CompanyValue("property", "property1",true, Source.AUTOMATIC_IMPORT);
    companyMaster.addValue(companyValue3);

    Instant dateTime = Instant.parse("2022-01-01T00:00:00.000Z");
    when(dateTimeProvider.getNow()).thenReturn(Optional.of(dateTime));

    repo.save(companyMaster);
  }

}