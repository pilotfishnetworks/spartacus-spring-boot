package se.pilotfish.spartacus.application.failure;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Data;

/**
 * Created by magnusl on 2017-05-29.
 */
@Data
@Builder
public class SpartacusFailureResult {

    @JsonProperty
    ErrorId errorId;

    @JsonProperty
    String message;

    @JsonProperty
    @Builder.Default
    Map<String, String> details = new HashMap<String, String>();

    public void addDetail(String key, String info){
        details.put(key, info);
    }

    public String detailValue(String key) {
        return details.get(key);
    }
}
