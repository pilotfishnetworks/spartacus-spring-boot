package se.pilotfish.spartacus.service;

import se.pilotfish.spartacusclient.rest.json.Message;

public interface VehicleModelMasterDataService {

  Message updateVehicleModelMaster(Message message);

}
