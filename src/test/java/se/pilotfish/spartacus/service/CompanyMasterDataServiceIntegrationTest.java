package se.pilotfish.spartacus.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.DateTimeProvider;
import se.pilotfish.spartacus.annotation.SpringBootDbIntegrationTest;
import se.pilotfish.spartacus.db.AbstractTestContainerIntegrationTest;
import se.pilotfish.spartacusclient.rest.json.Company;
import se.pilotfish.spartacusclient.rest.json.CustomerReference;
import se.pilotfish.spartacusclient.rest.json.Event;
import se.pilotfish.spartacusclient.rest.json.Message;
import se.pilotfish.spartacusclient.rest.json.Property;
import se.pilotfish.spartacusclient.rest.json.Source;

@SpringBootDbIntegrationTest
class CompanyMasterDataServiceIntegrationTest extends AbstractTestContainerIntegrationTest {

  @MockBean
  private DateTimeProvider dateTimeProvider;

  @SpyBean
  private AuditingHandler handler;

  @Inject
  private CompanyMasterDataServiceImpl service;

  @BeforeEach
  @DataSet("datasets/CustomerMaster.yml")
  void setUp() throws Exception {
    handler.setDateTimeProvider(dateTimeProvider);
  }

  @AfterEach
  @DataSet("datasets/clean.yml")
  void teardown() {
  }

  @Test
  @DataSet("datasets/CompanyMaster.yml")
  @ExpectedDataSet("datasets/updated_CompanyMaster.yml")
  void updateCompanyMaster() {
    Company companyEntity = new Company();
    companyEntity.setExternalIdentifier("externalIdentifier");
    companyEntity.setName("company1");
    companyEntity.setType("type2");
    CustomerReference customer = new CustomerReference();
    customer.setShortName("company1");
    companyEntity.setCustomer(customer);
    List<Property> properties = new ArrayList<>();
    Property property = new Property();
    property.setName("property2");
    property.setValue("newValue");
    property.setType("STRING");
    properties.add(property);
    companyEntity.setProperties(properties);
    Message message = Message.builder()
        .event(Event.CREATE_OR_UPDATE)
        .source(Source.CUSTOMER)
        .payload(companyEntity)
        .build();

    Instant dateTime = Instant.parse("2022-02-01T00:00:00.000Z");
    when(dateTimeProvider.getNow()).thenReturn(Optional.of(dateTime));

    Message outMessage = service.updateCompanyMaster(message);
    assertThat(outMessage.getPayload().getMeta().getUpdated()).isEqualTo(dateTime.toEpochMilli());
  }

}