package se.pilotfish.spartacus.kafka.producer;

import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import se.pilotfish.spartacusclient.rest.json.Message;

@Slf4j
@Component
public class MasterDataMessageProducerImpl implements MasterDataMessageProducer {

  private final KafkaTemplate<String, Message> kafkaTemplate;
  private final String masterDataTopic;

  @Inject
  public MasterDataMessageProducerImpl(
      KafkaTemplate<String, Message> kafkaTemplate, @Value("${kafka.topic.master-data.name}") String masterDataTopic) {
    this.kafkaTemplate = kafkaTemplate;
    this.masterDataTopic = masterDataTopic;
  }

  @Override
  public void publish(Message message) {
    kafkaTemplate.send(masterDataTopic, message.messageKey(), message);
    log.debug("sent master data message {}", message);
  }

}
