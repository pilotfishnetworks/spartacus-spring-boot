package se.pilotfish.spartacus.application.validation;

import se.pilotfish.spartacusclient.rest.json.Entity;

public interface Validator {

  boolean isValid(Entity entity);

  ValidationError validate(Entity entity);

}
