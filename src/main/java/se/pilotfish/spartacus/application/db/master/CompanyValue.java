package se.pilotfish.spartacus.application.db.master;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.Getter;
import lombok.Setter;
import se.pilotfish.spartacusclient.rest.json.Source;

@Entity
@Getter
@Setter
public class CompanyValue extends GenericMasterValue {

    @ManyToOne
    @MapsId("masterRecordIdentifier")
    private CompanyMaster masterCompany;

    public CompanyValue(){}

    public CompanyValue(String classification, String name, String value, Source source){
        super(classification, name, value, source);
    }

    public CompanyValue(String classification, String name, Integer value, Source source) {
        super(classification, name, value, source);
    }

    public CompanyValue(String classification, String name, Boolean value, Source source) {
        super(classification, name, value, source);
    }

    public CompanyValue(String classification, String name, Date value, Source source) {
        super(classification, name, value, source);
    }

    public CompanyValue(String classification, String name, String value,
        ValueType type, Source source) {
        super(classification, name, value, type, source);
    }

    @Override
    protected void remove() {
        this.masterCompany = null;
    }

}
