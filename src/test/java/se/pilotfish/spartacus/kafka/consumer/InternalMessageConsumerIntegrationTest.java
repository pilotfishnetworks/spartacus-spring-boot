package se.pilotfish.spartacus.kafka.consumer;

import static org.mockito.ArgumentMatchers.any;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.test.context.junit4.SpringRunner;
import se.pilotfish.spartacus.service.MasterDataService;
import se.pilotfish.spartacusclient.rest.json.Company;
import se.pilotfish.spartacusclient.rest.json.Message;

@RunWith(SpringRunner.class)
@SpringBootTest
@Tag("integration")
public class InternalMessageConsumerIntegrationTest {

  @Autowired
  private KafkaProperties kafkaProperties;

  @Autowired
  private KafkaTemplate<String, Message> kafkaTemplate;

  @Autowired
  private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

  @MockBean
  private MasterDataService masterDataService;

  @ClassRule
  public static EmbeddedKafkaRule embeddedKafkaRule = new EmbeddedKafkaRule(1, true, "spartacus-incoming-masterdata");

  @BeforeClass
  public static void setUpBeforeClass() {
      System.setProperty("spring.kafka.bootstrap-servers", embeddedKafkaRule.getEmbeddedKafka().getBrokersAsString());
  }

  @Before
  public void setUp() throws Exception {
    // wait until the partitions are assigned
    for (MessageListenerContainer messageListenerContainer : kafkaListenerEndpointRegistry
        .getListenerContainers()) {
      ContainerTestUtils.waitForAssignment(messageListenerContainer,
          embeddedKafkaRule.getEmbeddedKafka().getPartitionsPerTopic());
    }
  }

  @Test
  public void testMessageConsumer() throws Exception {
    Company company = new Company();
    company.setExternalIdentifier("identifier");
    Message message = new Message();
    message.setPayload(company);

    kafkaTemplate.send("spartacus-incoming-masterdata", message.messageKey(), message);

    Mockito.verify(masterDataService, Mockito.timeout(1000).only()).updateMasterData(any(Message.class));
  }

}