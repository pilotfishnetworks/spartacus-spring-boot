package se.pilotfish.spartacus.application.failure;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String CONTENT_TYPE = "application/json";

    @ExceptionHandler({SpartacusRuntimeException.class})
    protected ResponseEntity<Object> handleBaseException(SpartacusRuntimeException exception, WebRequest request) {

        if (exception.getStatusCode() >= HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            log.error("BaseException", exception);
        } else {
            log.trace("BaseException", exception);
        }
        SpartacusFailureResult body = SpartacusFailureResult.builder()
            .errorId(exception.getErrorId())
            .message(exception.getMessage())
            .build();
        return handleExceptionInternal(exception, body, getHttpHeaders(),
                HttpStatus.valueOf(exception.getStatusCode()), request);
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(CONTENT_TYPE));
        return headers;
    }

}
