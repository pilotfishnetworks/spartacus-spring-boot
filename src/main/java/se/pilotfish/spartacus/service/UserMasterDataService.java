package se.pilotfish.spartacus.service;

import se.pilotfish.spartacusclient.rest.json.Message;

public interface UserMasterDataService {

  Message updateUserMaster(Message message);

}
