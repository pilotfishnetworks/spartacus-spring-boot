package se.pilotfish.spartacus.application.db.master;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by magnusl on 2018-05-09.
 */
@Entity
@Table(uniqueConstraints =
        @UniqueConstraint(name = "UserMaster_customerShortName_externalIdentifier_idx",
                          columnNames = {"customerShortName", "externalIdentifier"}))
@Getter
@Setter
@NoArgsConstructor
public class UserMaster extends GenericMasterRecord {

    @NotNull
    private String customerShortName;

    private String externalIdentifier;

    @OneToMany(mappedBy = "masterUser", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @MapKey
    private Map<GenericMasterValueId, UserValue> userValues = new HashMap<>();

    @Builder
    public UserMaster(UUID identifier, @NotNull String customerShortName, String externalIdentifier, Boolean hidden) {
        super(identifier, hidden);
        this.customerShortName = customerShortName;
        this.externalIdentifier = externalIdentifier;
    }

    @Override
    public Map<GenericMasterValueId, ? extends GenericMasterValue> getValues() {
        return userValues;
    }

    @Override
    public void addValue(GenericMasterValue updatedValue) {
        UserValue updatedUserValue = (UserValue) updatedValue;
        updatedUserValue.setMasterUser(this);
        userValues.put(updatedValue.getIdentifier(), updatedUserValue);
    }

    @Override
    public void removeValue(GenericMasterValue removedValue) {
        UserValue removedUserValue =  userValues.remove(removedValue.getIdentifier());
        if (removedUserValue != null) {
            removedUserValue.remove();
        }
    }

}
